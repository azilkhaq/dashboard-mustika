<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;
use Config\Services;

class SenderController extends BaseController
{
    public function index()
    {
        return view('sender/index');
    }

    public function getData()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/senderid/filter?size=2000', 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function create()
    {
        return view('sender/create');
    }

    public function postCreate()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $cpName = $request->getPost('cpName');
        $sender = $request->getPost('sender');
        $sid = $request->getPost('sid');
        $url = $request->getPost('url');
        $pwd = $request->getPost('pwd');
        $owner = $request->getPost('owner');
        $type = $request->getPost('type');

        $body = [
            "cpName" => $cpName,
            "sender" => $sender,
            "sid"    => $sid,
            "url"    => $url,
            "pwd"    => $pwd,
            "owner"  => $owner,
            "type"   => $type
        ];

        $url = getenv('API_URL') . '/api/v1/senderid/management/add';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function edit($senderId)
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/senderid/fetch/' . $senderId, 'GET');

        $data['data'] = $result->body;

        return view('sender/edit', $data);
    }

    public function postEdit()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $Id = $request->getPost('Id');
        $cpName = $request->getPost('cpName');
        $sender = $request->getPost('sender');
        $sid = $request->getPost('sid');
        $url = $request->getPost('url');
        $pwd = $request->getPost('pwd');
        $owner = $request->getPost('owner');
        $type = $request->getPost('type');

        $body = [
            "id" => $Id,
            "cpName" => $cpName,
            "sender" => $sender,
            "sid"    => $sid,
            "url"    => $url,
            "pwd"    => $pwd,
            "owner"  => $owner,
            "type"   => $type
        ];

        $url = getenv('API_URL') . '/api/v1/senderid/management/update';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function delete()
    {
        $request = Services::request();
        $session = Services::session();
        $client = new \GuzzleHttp\Client(['verify' => false]);

        $Id = $request->getPost('Id');
        $response = $client->delete(getenv('API_URL') . '/api/v1/senderid/management/delete/' . $Id, [
            'headers' =>  [
                'Authorization' => 'Bearer ' . $session->get('token')
            ]
        ]);
    }
}
