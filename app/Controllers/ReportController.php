<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;
use Config\Services;


class ReportController extends BaseController
{
    public function smsLogs()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/report/index/prefixes', 'GET');

        $data['index'] = $result->body;

        return view('report/smsLogs', $data);
    }

    public function getDataSmsLogs()
    {
        $request = Services::request();
        $paramsIndex = "";
        $paramsPrefix = "";

        if (session("role") == "PARTNER") {
            $indexs = session("indexs");
            $paramsIndex .= 'indexes=' . $indexs . '&';
        } else {
            $indexs = $request->getPost('indexs');
            $splitIndex = explode(",", $indexs);

            for ($i = 0; $i < count($splitIndex); $i++) {
                $paramsIndex .= 'indexes=' . $splitIndex[$i] . '&';
            }
        }

        $start = $request->getPost('start');
        $end = $request->getPost('end');
        $search = $request->getPost('search');
        $msisdn = $request->getPost('msisdn');
        $sid = $request->getPost('sid');
        $cpname = $request->getPost('cpname');
        $trxid = $request->getPost('trxid');
        $prefix = $request->getPost('prefix');

        if ($prefix != "") {
            $splitPrefix = explode(",", $prefix);
            for ($p = 0; $p < count($splitPrefix); $p++) {
                $paramsPrefix .= '&prefix=' . $splitPrefix[$p];
            }
        }

        $result = curlHelper(getenv('API_URL') . '/api/v1/report/filter?' . $paramsIndex . 'startTime=' . $start . 'T00:00:00&endTime=' . $end . 'T23:59:59&search=' . $search . '&msisdn=' . $msisdn . '&sid=' . $sid . '&cpname=' . $cpname . '&trxid=' . $trxid . $paramsPrefix . '&size=2000', 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function trafficTime()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/report/index/prefixes', 'GET');

        $data['index'] = $result->body;

        return view('report/trafficTime', $data);
    }

    public function getDataTrafficTime()
    {
        $request = Services::request();

        if (session("role") == "PARTNER") {
            $indexs = session("indexs");
        } else {
            $indexs = $request->getPost('indexs');
        }

        $filter = $request->getPost('filter');
        $start = $request->getPost('start');
        $end = $request->getPost('end');

        $result = curlHelper(getenv('API_URL') . '/api/v1/report/histogram/1' . $filter . '/response?indexes=' . $indexs . '&startTime=' . $start . '&endTime=' . $end, 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function trafficSender()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/report/index/prefixes', 'GET');

        $data['index'] = $result->body;

        return view('report/trafficSender', $data);
    }

    public function getDataTrafficSender()
    {
        $request = Services::request();
        $paramsIndex = "";
        $paramsPrefix = "";

        if (session("role") == "PARTNER") {
            $indexs = session("indexs");
            $paramsIndex .= 'indexes=' . $indexs . '&';

            $prefix = session("prefix");

            if ($prefix != "") {
                $paramsPrefix .= '&prefix=' . $prefix;
            }
        } else {
            $indexs = $request->getPost('indexs');
            $splitIndex = explode(",", $indexs);

            for ($i = 0; $i < count($splitIndex); $i++) {
                $paramsIndex .= 'indexes=' . $splitIndex[$i] . '&';
            }

            $prefix = $request->getPost('prefix');

            if ($prefix != "") {
                $splitPrefix = explode(",", $prefix);
                for ($p = 0; $p < count($splitPrefix); $p++) {
                    $paramsPrefix .= '&prefix=' . $splitPrefix[$p];
                }
            }
        }

        $filter = $request->getPost('filter');
        $start = $request->getPost('start');
        $end = $request->getPost('end');
        $sid = $request->getPost('sid');

        $result = curlHelper(getenv('API_URL') . '/api/v1/report/histogram/1' . $filter . '/response?' . $paramsIndex . 'startTime=' . $start . '&sid=' . $sid . '&endTime=' . $end . $paramsPrefix, 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function trafficSummary()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/report/index/prefixes', 'GET');

        $data['index'] = $result->body;

        return view('report/trafficSummary', $data);
    }

    public function getDataTrafficSummary()
    {
        $request = Services::request();

        $paramsIndex = "";
        $paramsPrefix = "";

        if (session("role") == "PARTNER") {
            $indexs = session("indexs");
            $paramsIndex .= 'indexes=' . $indexs . '&';

            $prefix = session("prefix");

            if ($prefix != "") {
                $paramsPrefix .= '&prefix=' . $prefix;
            }
        } else {
            $indexs = $request->getPost('indexs');
            $splitIndex = explode(",", $indexs);

            for ($i = 0; $i < count($splitIndex); $i++) {
                $paramsIndex .= 'indexes=' . $splitIndex[$i] . '&';
            }

            $prefix = $request->getPost('prefix');

            if ($prefix != "") {
                $splitPrefix = explode(",", $prefix);
                for ($p = 0; $p < count($splitPrefix); $p++) {
                    $paramsPrefix .= '&prefix=' . $splitPrefix[$p];
                }
            }
        }

        $start = $request->getPost('start');
        $end = $request->getPost('end');
        $sid = $request->getPost('sid');

        $paramSid = "";

        if ($sid != "") {
            $paramSid = '&sid=' . $sid;
        }

        $result = curlHelper(getenv('API_URL') . '/api/v1/report/countGroupByPrefixSidAndResponse?' . $paramsIndex . 'startTime=' . $start . '&endTime=' . $end  . $paramsPrefix . '&size=2000' . $paramSid, 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function trafficInternational()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/report/index/prefixes', 'GET');

        $data['index'] = $result->body;

        return view('report/trafficInternational', $data);
    }

    public function getDataTrafficInternational()
    {
        $request = Services::request();

        $start = $request->getPost('start');
        $end = $request->getPost('end');
        $clientId = $request->getPost('client');
        $sender = $request->getPost('sender');

        if (session("role") == "PARTNER") { 
            $clientId = session("clientId");
        }

        $result = curlHelper(getenv('API_URL') . '/api/v1/report/intl/summary?start=' . $start . '%2000:00:00&end=' . $end . '%2023:59:59&sender=' . $sender . '&clientId=' . $clientId, 'GET');

        $array = [];

        foreach ($result->body as $row) {

            foreach ($row->counts as $count) {

                $meta = array(
                    "name" => $row->name,
                    "sender" => $count->sender,
                    "status" => $count->status,
                    "value" => $count->value,
                );

                array_push($array, $meta);
            }
        }

        return json_encode([
            "body" =>  $array
        ]);
    }

    public function getPrefix()
    {
        $request = Services::request();

        $indexs = $request->getPost('value');

        $result = curlHelper(getenv('API_URL') . '/api/v1/report/index/prefixes', 'GET');

        $arrayIndexs = [];
        $resultArray = [];

        foreach ($result->body as $row) {

            $array = array(
                "pattern" => $row->pattern,
                "indexes" => $row->indexes,
                "prefixes" => $row->prefixes
            );

            array_push($arrayIndexs, $array);
        }

        $split = explode(",", $indexs);

        for ($i = 0; $i < count($split); $i++) {

            $id = searchForId($split[$i], $arrayIndexs);

            array_push($resultArray, $arrayIndexs[$id]);
        }

        $resultData = [];

        if ($indexs == "*") {
            $resultData = $arrayIndexs;
        } else {
            $resultData = $resultArray;
        }

        return json_encode([
            "body" =>  $resultData
        ]);
    }
}
