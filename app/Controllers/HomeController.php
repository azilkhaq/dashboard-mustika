<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;

class HomeController extends BaseController
{
    public function index()
    {
        return view('home/index');
    }

    public function getData()
    {
        if (session("role") == "PARTNER") {
            $indexes = session("indexs");
        } else {
            $indexes = "*";
        }

        $now = date('Y-m-d');

        $count = curlHelper(getenv('API_URL') . '/api/v1/report/count?indexes=' . $indexes . '&startTime=' . $now . 'T00:00:00&endTime=' . $now . 'T23:59:59', 'GET');

        $sum = curlHelper(getenv('API_URL') . '/api/v1/report/sum?indexes=' . $indexes . '&startTime=' . $now . 'T00:00:00&endTime=' . $now . 'T23:59:59', 'GET');

        return json_encode([
            "count" =>  $count->body->value,
            "sum" =>  $sum->body->value,
        ]);
    }
}
