<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;
use Config\Services;

class ConfigProfileController extends BaseController
{
    public function index()
    {
        return view('configProfile/index');
    }

    public function getData()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/sender/config/profile/filter?size=2000', 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function create()
    {
        return view('configProfile/create');
    }

    public function postCreate()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $name = $request->getPost('name');
        $dataCenter = $request->getPost('dataCenter');
        $dbHost = $request->getPost('dbHost');
        $dbName = $request->getPost('dbName');
        $tableName = $request->getPost('tableName');
        $username = $request->getPost('username');
        $password = $request->getPost('password');
        $remark = $request->getPost('remark');

        $body = [
            "name" => $name,
            "dataCenter" => $dataCenter,
            "dbHost" => $dbHost,
            "dbName" => $dbName,
            "tableName" => $tableName,
            "username" =>  $username,
            "password" => $password,
            "remark" => $remark
        ];

        $url = getenv('API_URL') . '/api/v1/sender/config/profile/add';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function edit($userId)
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/sender/config/profile/' . $userId, 'GET');

        $data['data'] = $result->body;

        return view('configProfile/edit', $data);
    }

    public function postEdit()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $name = $request->getPost('name');
        $dataCenter = $request->getPost('dataCenter');
        $dbHost = $request->getPost('dbHost');
        $dbName = $request->getPost('dbName');
        $tableName = $request->getPost('tableName');
        $username = $request->getPost('username');
        $password = $request->getPost('password');
        $remark = $request->getPost('remark');

        $body = [
            "name" => $name,
            "dataCenter" => $dataCenter,
            "dbHost" => $dbHost,
            "dbName" => $dbName,
            "tableName" => $tableName,
            "username" =>  $username,
            "password" => $password,
            "remark" => $remark
        ];

        $url = getenv('API_URL') . '/api/v1/sender/config/profile/update';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }
}
