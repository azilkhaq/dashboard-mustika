<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;
use Config\Services;

class ClientsController extends BaseController
{
    public function index()
    {
        return view('clients/index');
    }

    public function getData()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/client/filter?size=2000', 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function getDetail($userId)
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/client/fetch/' . $userId, 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function create()
    {
        return view('clients/create');
    }

    public function postCreate()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $fullname = $request->getPost('fullname');
        $userId = $request->getPost('userId');
        $password = $request->getPost('password');
        $email = $request->getPost('email');
        $role = $request->getPost('role');
        $senderId = $request->getPost('senderId');
        $configProfileNames = $request->getPost('configProfileNames');
        $prefixes = $request->getPost('prefixes');
        $path = '';

        if (isset($_FILES['profilePic'])) {

            $filename =  $_FILES['profilePic']['name'];
            $rand = generateRandomString();
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $final = "/" . $rand . "." . $ext;
            $tmp = $_FILES['profilePic']['tmp_name'];
            $dirUpload = "uploads/";
            $upload = move_uploaded_file($tmp, $dirUpload . $final);

            if ($upload) {
                $path = $final;
            } else {
                $path = '';
            }
        }

        $body = [
            "clientId" => $userId,
            "user" => array(
                "userId" => $userId,
                "fullname" => $fullname,
                "password" => $password,
                "email" => $email,
                "profilePic" =>  $path,
                "role" => $role
            ),
            "configProfileNames" => explode(",", $configProfileNames),
            "senderIds" => explode(",", $senderId),
            "prefixes" => json_decode($prefixes),
        ];

        $url = getenv('API_URL') . '/api/v1/client/management/add';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function edit($userId)
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/client/fetch/' . $userId, 'GET');
        $profile = curlHelper(getenv('API_URL') . '/api/v1/sender/config/profile/filter?size=2000', 'GET');
        $sender = curlHelper(getenv('API_URL') . '/api/v1/senderid/filter?size=2000', 'GET');

        $data['profile'] = $profile->body;
        $data['sender'] = $sender->body;
        $data['data'] = $result->body;

        return view('clients/edit', $data);
    }

    public function postEdit()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $clientId = $request->getPost('clientId');
        $fullname = $request->getPost('fullname');
        $userId = $request->getPost('userId');
        $password = $request->getPost('password');
        $email = $request->getPost('email');
        $role = $request->getPost('role');
        $senderId = $request->getPost('senderId');
        $configProfileNames = $request->getPost('configProfileNames');
        $prefixes = $request->getPost('prefixes');
        $path = '';

        if (isset($_FILES['profilePic'])) {

            $filename =  $_FILES['profilePic']['name'];
            $rand = generateRandomString();
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $final = "/" . $rand . "." . $ext;
            $tmp = $_FILES['profilePic']['tmp_name'];
            $dirUpload = "uploads/";
            $upload = move_uploaded_file($tmp, $dirUpload . $final);

            if ($upload) {
                $path = $final;
            } else {
                $path = '';
            }
        }

        $body = [
            "clientId" => $clientId,
            "user" => array(
                "userId" => $userId,
                "fullname" => $fullname,
                "password" => $password,
                "email" => $email,
                "profilePic" =>  $path,
            ),
            "configProfileNames" => explode(",", $configProfileNames),
            "senderIds" => explode(",", $senderId),
            "prefixes" => json_decode($prefixes),
        ];

        $url = getenv('API_URL') . '/api/v1/client/management/update';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function delete()
    {
        $request = Services::request();
        $session = Services::session();
        $client = new \GuzzleHttp\Client(['verify' => false]);

        $Id = $request->getPost('Id');
        $response = $client->delete(getenv('API_URL') . '/api/v1/client/management/delete/' . $Id, [
            'headers' =>  [
                'Authorization' => 'Bearer ' . $session->get('token')
            ]
        ]);
    }

    public function detail($userId)
    {
        $data['clientId'] = $userId;

        return view('clients/detail', $data);
    }

    public function postAddSender()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $clientId = $request->getPost('clientId');
        $senderId = $request->getPost('senderId');

        $body = [
            "clientId" => $clientId,
            "senderId" => $senderId,
        ];

        $url = getenv('API_URL') . '/api/v1/client/sender/assign';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function deleteSender()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $clientId = $request->getPost('clientId');
        $senderId = $request->getPost('senderId');

        $body = [
            "clientId" => $clientId,
            "senderId" => $senderId,
        ];

        $url = getenv('API_URL') . '/api/v1/client/sender/unassign';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function postAddProfile()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $clientId = $request->getPost('clientId');
        $configProfileNames = $request->getPost('configProfileNames');

        $body = [
            "clientId" => $clientId,
            "profileName" => $configProfileNames,
        ];

        $url = getenv('API_URL') . '/api/v1/client/profile/assign';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function deleteProfile()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $clientId = $request->getPost('clientId');
        $configProfileNames = $request->getPost('configProfileNames');

        $body = [
            "clientId" => $clientId,
            "profileName" => $configProfileNames,
        ];

        $url = getenv('API_URL') . '/api/v1/client/profile/unassign';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }
}
