<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;
use Config\Services;

class ConfigSenderController extends BaseController
{
    public function index()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/sender/config/profile/filter?size=2000', 'GET');

        $data['data'] = $result->body;

        return view('configSender/index', $data);
    }

    public function getData($name)
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/sender/config/filter?profileName=' . $name .'&size=2000', 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function create()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/sender/config/profile/filter?size=2000', 'GET');

        $data['data'] = $result->body;

        return view('configSender/create', $data);
    }

    public function postCreate()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $profileName = $request->getPost('profileName');
        $cpName = $request->getPost('cpName');
        $sender = $request->getPost('sender');
        $sid = $request->getPost('sid');
        $urlSender = $request->getPost('url');
        $pwd = $request->getPost('pwd');
        $owner = $request->getPost('owner');
        $type = $request->getPost('type');

        $body = [
            "profileName" => $profileName,
            "cpName" => $cpName,
            "sender" => $sender,
            "sid" => $sid,
            "url" => $urlSender,
            "pwd" => $pwd,
            "owner" => $owner,
            "type" => $type
        ];

        $url = getenv('API_URL') . '/api/v1/sender/config/add';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }
}
