<?php

namespace App\Controllers\Auth;

use App\Controllers\Base\BaseController;
use Config\Services;
use GuzzleHttp\Client;

class LoginController extends BaseController
{
    public function index()
    {
        return view("auth/login");
    }

    public function store()
    {
        $data = array();
        $request = Services::request();
        $session = Services::session();

        $username = $request->getPost('username');
        $password = $request->getPost('password');

        $client = new Client([\GuzzleHttp\RequestOptions::VERIFY  => false]);

        $data = array(
            'username' => $username,
            'password' => $password
        );

        try {

            $url = getenv('API_URL') . '/auth/v1/login';

            $req = $client->post($url, [
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                'body'    => json_encode($data)
            ]);

        } catch (\Exception $e) {

            $data["message"] = "Username atau password salah";
            $session->set($data);
            
            return redirect()->to(base_url('auth/login'));
        }

        $response = $req->getBody()->getContents();
        $result = json_decode($response);

        $data = array();
        $data["token"] = $result->body->accessToken;
        $data["username"] = $result->body->user->fullname;
        $data["userId"] = $result->body->user->userId;
        $data["role"] = $result->body->user->role;
        $data["profilePic"] = $result->body->user->profilePic;
        $data["authenticated"] = true;
        $data["success"] = true;

        if (json_encode($result->body->user->info) != "{}") {
            $data["indexs"] = $result->body->user->info->prefixes[0]->pattern;
            $data["prefix"] = $result->body->user->info->prefixes[0]->prefix;
            $data["clientId"] = $result->body->user->info->clientId;
        }

        $session->set($data);
    }

    public function logout()
    {
        $session = Services::session();
        $session->remove('token');
        $session->remove('unionId');
        $session->remove('username');
        $session->remove('userId');
        $session->remove('role');
        $session->remove('profilePic');
        $session->remove('authenticated');
        $session->remove('success');
        $session->remove('indexs');
        $session->remove('prefix');

        return redirect()->to(base_url('auth/login'));
    }
}
