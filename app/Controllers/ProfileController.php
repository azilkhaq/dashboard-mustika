<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;
use Config\Services;

class ProfileController extends BaseController
{
    public function index()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/user/profile', 'GET');
        $data['profile'] = $result->body;

        return view('profile/index', $data);
    }

    public function changePassword()
    {
        $client = new \GuzzleHttp\Client();
        $session = Services::session();
        $request = Services::request();

        $oldPassword = $request->getPost('oldPassword');
        $newPassword = $request->getPost('newPassword');
        $confirmPassword = $request->getPost('confirmPassword');


        $url = getenv('API_URL') . '/api/v1/user/changePassword';

        $body = [
            "oldPassword" => $oldPassword,
            "newPassword" => $newPassword,
            "confirmPassword" => $confirmPassword,
        ];

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function update()
    {
        $client = new \GuzzleHttp\Client();
        $session = Services::session();
        $request = Services::request();

        $fullname = $request->getPost('fullname');
        $email = $request->getPost('email');
        $path = '';

        if (isset($_FILES['profilePic'])) {

            $filename =  $_FILES['profilePic']['name'];
            $rand = generateRandomString();
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $final = "/" . $rand . "." . $ext;
            $tmp = $_FILES['profilePic']['tmp_name'];
            $dirUpload = "uploads/";
            $upload = move_uploaded_file($tmp, $dirUpload . $final);

            if ($upload) {
                $path = $final;
            } else {
                $path = '';
            }
        }

        $url = getenv('API_URL') . '/api/v1/user/update';
        $body = [
            "fullname" => $fullname,
            "email" => $email,
            "profilePic" => $path,
        ];

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }
}
