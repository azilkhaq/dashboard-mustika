<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;
use Config\Services;

class UsersController extends BaseController
{
    public function index()
    {
        return view('users/index');
    }

    public function getData()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/user/filter?size=2000', 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function getDetail($userId)
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/user/profile/' . $userId, 'GET');

        return json_encode([
            "body" =>  $result->body
        ]);
    }

    public function create()
    {
        return view('users/create');
    }

    public function postCreate()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $fullname = $request->getPost('fullname');
        $username = $request->getPost('username');
        $password = $request->getPost('password');
        $email = $request->getPost('email');
        $role = $request->getPost('role');
        $clientId = $request->getPost('clientId');
        $prefix = $request->getPost('prefix');
        $typePartner = $request->getPost('typePartner');
        $pattern = $request->getPost('pattern');
        $remark = $request->getPost('remark');
        $path = '';

        if (isset($_FILES['profilePic'])) {

            $filename =  $_FILES['profilePic']['name'];
            $rand = generateRandomString();
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $final = "/" . $rand . "." . $ext;
            $tmp = $_FILES['profilePic']['tmp_name'];
            $dirUpload = "uploads/";
            $upload = move_uploaded_file($tmp, $dirUpload . $final);

            if ($upload) {
                $path = $final;
            } else {
                $path = '';
            }
        }

        if ($role == "PARTNER") {
            $body = [
                "userId" => $username,
                "fullname" => $fullname,
                "password" => $password,
                "email" => $email,
                "profilePic" =>  $path,
                "role" => $role,
                "status" => 1,
                "client" => array(
                    "clientId" => $clientId,
                    "prefixes" => [
                        array(
                            "prefix" => $prefix,
                            "type" => $typePartner,
                            "pattern" => $pattern,
                            "remark" => $remark
                        ),
                    ],
                ),
            ];
        } else {
            $body = [
                "userId" => $username,
                "fullname" => $fullname,
                "password" => $password,
                "email" => $email,
                "profilePic" =>  $path,
                "role" => $role,
                "status" => 1
            ];
        }

        $url = getenv('API_URL') . '/api/v1/user/management/add';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function edit($userId)
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/user/profile/' . $userId, 'GET');

        $data['data'] = $result->body;

        return view('users/edit', $data);
    }

    public function postEdit()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $session = Services::session();
        $request = Services::request();

        $userId = $request->getPost('userId');
        $fullname = $request->getPost('fullname');
        $password = $request->getPost('password');
        $email = $request->getPost('email');
        $valProfilePic = $request->getPost('valProfilePic');
        $path = $valProfilePic;
        $role = $request->getPost('role');
        $clientId = $request->getPost('clientId');
        $prefix = $request->getPost('prefix');
        $typePartner = $request->getPost('typePartner');
        $pattern = $request->getPost('pattern');
        $remark = $request->getPost('remark');

        if (isset($_FILES['profilePic'])) {

            $filename =  $_FILES['profilePic']['name'];
            $rand = generateRandomString();
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $final = "/" . $rand . "." . $ext;
            $tmp = $_FILES['profilePic']['tmp_name'];
            $dirUpload = "uploads/";
            $upload = move_uploaded_file($tmp, $dirUpload . $final);

            if ($upload) {
                $path = $final;
            } else {
                $path = '';
            }
        }

        if ($role == "PARTNER") {
            $body = [
                "userId" => $userId,
                "fullname" => $fullname,
                "password" => $password,
                "email" => $email,
                "profilePic" =>  $path,
                "status" => 1,
                "client" => array(
                    "clientId" => $clientId,
                    "prefixes" => [
                        array(
                            "prefix" => $prefix,
                            "type" => $typePartner,
                            "pattern" => $pattern,
                            "remark" => $remark
                        ),
                    ],
                ),
            ];
        } else {
            $body = [
                "userId" => $userId,
                "fullname" => $fullname,
                "password" => $password,
                "email" => $email,
                "profilePic" => $path,
                "status" => 1,
            ];
        }       

        $url = getenv('API_URL') . '/api/v1/user/management/update';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => 'Bearer ' . $session->get('token'),
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }
}
