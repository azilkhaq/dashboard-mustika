<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Config Sender</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Config Sender</li>
                                <li class="breadcrumb-item">Add Config Sender</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label class="form-label">Select Profile :</label>
                                        <select class="form-control select2" id="name">
                                            <option value="">Select</option>
                                            <?php foreach ($data as $row) : ?>
                                                <option value="<?= $row->name ?>"><?= $row->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 align-self-end">
                                    <div class="mb-3">
                                        <button type="button" class="btn btn-primary w-md" onclick="SubmitForm()" id="submitForm">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-sender" style="display: none;">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Form Add Config Sender</h4>

                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Profile Name</label>
                                            <input type="text" class="form-control" id="profileName" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>CP Name</label>
                                            <input type="text" class="form-control" id="cpName" placeholder="Enter cpName">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Sender</label>
                                            <input type="text" class="form-control" id="sender" placeholder="Enter sender">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>SID</label>
                                            <input type="text" class="form-control" id="sid" placeholder="Enter sid">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>URL</label>
                                            <input type="text" class="form-control" id="url" placeholder="Enter url">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>PWD</label>
                                            <input type="text" class="form-control" id="pwd" placeholder="Enter pwd">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Owner</label>
                                            <input type="text" class="form-control" id="owner" placeholder="Enter owner">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Type</label>
                                            <input type="text" class="form-control" id="type" placeholder="Enter type">
                                        </div>
                                    </div>
                                </div>

                                <div class="button-form">
                                    <a href="<?= base_url('configSender') ?>" class="btn btn-danger w-md"><i class="bx bx-x-circle font-size-20 align-middle me-2"></i> Batal</a>&nbsp;
                                    <button type="button" class="btn btn-primary w-md" onclick="Create()" id="create"><i class="bx bx-send font-size-20 align-middle me-2"></i> Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/configSender'); ?>