<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Config Sender</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Config Sender</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row row-button">
                <div class="col-md-12">
                    <a href="<?= base_url("configSender/create") ?>" class="btn btn-primary waves-effect waves-light"><i class="bx bx-user-plus font-size-20 align-middle me-2"></i> Add Config Sender</a>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label class="form-label">Select Profile :</label>
                                        <select class="form-control select2" id="name">
                                            <option>Select</option>
                                            <?php foreach ($data as $row) : ?>
                                                <option value="<?= $row->name ?>"><?= $row->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 align-self-end">
                                    <div class="mb-3">
                                        <button type="button" class="btn btn-primary w-md" onclick="Submit()" id="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="loader-table" style="padding-bottom: 30px; display:none;">
                                <img src="<?= base_url("public/assets/images/loader.gif") ?>" alt="spinner" width="80" style="position: absolute; padding-top: 88px; margin-left: 40%;">
                            </div>

                            <table id="data" class="display nowrap table table-bordered  nowrap w-100" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Profile Name</th>
                                        <th>CP Name</th>
                                        <th>Sender</th>
                                        <th>SID</th>
                                        <th>URL</th>
                                        <th>PWD</th>
                                        <th>Owner</th>
                                        <th>Type</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/configSender'); ?>