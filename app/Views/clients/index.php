<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Managament Clients</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Managament Clients</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row row-button">
                <div class="col-md-12">
                    <a href="<?= base_url("clients/create") ?>" class="btn btn-primary waves-effect waves-light"><i class="bx bx-user-plus font-size-20 align-middle me-2"></i> Add Clients</a>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="loader-table" style="padding-bottom: 30px;">
                                <img src="<?= base_url("public/assets/images/loader.gif") ?>" alt="spinner" width="80" style="position: absolute; padding-top: 88px; margin-left: 40%;">
                            </div>

                            <table id="data" class="table table-bordered dt-responsive  nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Client ID</th>
                                        <th>Fullname</th>
                                        <th>Email</th>
                                        <th>Created Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/clients'); ?>

<script>
     Data();
</script>