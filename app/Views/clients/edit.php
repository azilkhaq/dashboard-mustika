<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Edit Clients</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Managament Clients</li>
                                <li class="breadcrumb-item">Edit Clients</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Form Edit Clients</h4>

                            <input type="text" id="clientId" value="<?= $data->clientId ?>" hidden>

                            <div class="image-upload">
                                <center>
                                    <div class="col-md-3">
                                        <?php if ($data->user->profilePic != "") { ?>
                                            <img src="<?= base_url("uploads") . $data->user->profilePic ?>" alt="image" id="showImage" style="height: 270px;">
                                            <input type="text" value="<?= $data->user->profilePic ?>" id="valProfilePic" hidden>
                                        <?php } else { ?>
                                            <img src="<?= base_url('public/assets/images/default.gif') ?>" alt="image" id="showImage">
                                        <?php } ?>
                                        <input type="file" class="form-control" id="image">
                                    </div>
                                </center>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>User ID</label>
                                        <input type="text" class="form-control" id="userId" value="<?= $data->user->userId ?>" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Fullname</label>
                                        <input type="text" class="form-control" id="fullname" value="<?= $data->user->fullname ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Email</label>
                                        <input type="text" class="form-control" id="email" value="<?= $data->user->email ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Role</label>
                                        <select id="role" class="form-control">
                                            <option value="PARTNER">Partner</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Password</label>
                                        <input type="text" class="form-control" id="password" placeholder="Enter password...">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Confirm Password</label>
                                        <input type="text" class="form-control" id="confirmPassword" placeholder="Enter confirm password...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Sender ID</h4>

                            <div class="col-md-6">
                                <select class="select2 form-control select2-multiple" multiple="multiple" id="senderId" data-placeholder="Choose senderId...">
                                    <?php foreach ($sender as $rowSender) : ?>
                                        <option value="<?= $rowSender->id ?>" <?= searchForSender($rowSender->id, $data->senders) ? "selected" : "" ?>><?= $rowSender->sender ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Config Profile Names</h4>

                            <div class="col-md-6">
                                <select class="select2 form-control select2-multiple" id="configProfileNames" multiple="multiple" data-placeholder="Choose config profile names...">
                                    <?php foreach ($profile as $row) : ?>
                                        <option value="<?= $row->name ?>" <?= in_array($row->name, $data->configProfiles) ? "selected" : "" ?>><?= $row->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Customer</h4>

                            <div class="outer-repeater">
                                <div data-repeater-list="outer-group" class="outer">
                                    <div data-repeater-item class="outer">

                                        <div class="inner-repeater mb-4">
                                            <div data-repeater-list="inner-group" class="inner mb-3">


                                                <?php if ($data->prefixes) { ?>

                                                    <?php foreach ($data->prefixes as $prefixes) : ?>

                                                        <div data-repeater-item class="inner item-prefixes mb-3 row">
                                                            <div class="col-md-2">
                                                                <label>Customer</label>
                                                                <input type="text" name="prefix" class="inner form-control" value="<?= $prefixes->prefix ?>" />
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label>Type</label>
                                                                <input type="text" name="type" class="inner form-control" value="<?= $prefixes->type ?>" />
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label>Pattern</label>
                                                                <input type="text" name="pattern" class="inner form-control" value="<?= $prefixes->pattern ?>" />
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label>Remark</label>
                                                                <input type="text" name="remark" class="inner form-control" value="<?= $prefixes->remark ?>" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <label style="color: white;">Button</label>
                                                                <div class="d-grid">
                                                                    <input data-repeater-delete type="button" class="btn btn-primary inner" value="Delete" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php endforeach; ?>
                                                <?php } else { ?>

                                                    <div data-repeater-item class="inner item-prefixes mb-3 row">
                                                        <div class="col-md-2">
                                                            <label>Customer</label>
                                                            <input type="text" name="prefix" class="inner form-control" placeholder="Enter prefix..." />
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Type</label>
                                                            <input type="text" name="type" class="inner form-control" placeholder="Enter type..." />
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Pattern</label>
                                                            <input type="text" name="pattern" class="inner form-control" placeholder="Enter pattern..." />
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Remark</label>
                                                            <input type="text" name="remark" class="inner form-control" placeholder="Enter remark..." />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <label style="color: white;">Button</label>
                                                            <div class="d-grid">
                                                                <input data-repeater-delete type="button" class="btn btn-primary inner" value="Delete" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php } ?>
                                            </div>
                                            <input data-repeater-create type="button" class="btn btn-success inner" value="Add" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="button-form">
                                <a href="<?= base_url('clients') ?>" class="btn btn-danger w-md"><i class="bx bx-x-circle font-size-20 align-middle me-2"></i> Batal</a>&nbsp;
                                <button type="button" class="btn btn-primary w-md" onclick="Edit()" id="edit"><i class="bx bx-send font-size-20 align-middle me-2"></i> Submit</button>
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>


        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/clients'); ?>