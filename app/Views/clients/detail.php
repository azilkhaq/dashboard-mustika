<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Detail Clients</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Managament Clients</li>
                                <li class="breadcrumb-item">Detail Clients</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row row-button">
                <div class="col-md-12">
                    <a href="<?= base_url('clients') ?>" class="btn btn-primary w-md"><i class="bx bx-share font-size-20 align-middle me-2"></i> Back</a>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title"><?= $clientId ?></h4>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#prefix" role="tab" aria-selected="true">
                                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                        <span class="d-none d-sm-block">Prefixes</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#config" role="tab" aria-selected="false">
                                        <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                        <span class="d-none d-sm-block">Config Profile</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#sender" role="tab" aria-selected="false">
                                        <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                        <span class="d-none d-sm-block">Sender</span>
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content p-3 text-muted">

                                <div class="tab-pane active" id="prefix" role="tabpanel">
                                    <div class="loader-table" style="padding-bottom: 30px;">
                                        <img src="<?= base_url("public/assets/images/loader.gif") ?>" alt="spinner" width="80" style="position: absolute; padding-top: 88px; margin-left: 40%;">
                                    </div>

                                    <table id="dataPrefix" class="table table-bordered dt-responsive  nowrap w-100">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Customer</th>
                                                <th>Type</th>
                                                <th>Pattern</th>
                                                <th>Remark</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                                <div class="tab-pane" id="config" role="tabpanel">

                                    <div class="row row-button tab-button">
                                        <div class="col-md-12">
                                            <a type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#addProfile"><i class="bx bx-user-plus font-size-20 align-middle me-2"></i> Add Config profile</a>
                                        </div>
                                    </div>

                                    <div class="loader-table" style="padding-bottom: 30px;">
                                        <img src="<?= base_url("public/assets/images/loader.gif") ?>" alt="spinner" width="80" style="position: absolute; padding-top: 88px; margin-left: 40%;">
                                    </div>

                                    <table id="dataConfig" class="table table-bordered nowrap w-100">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Config Profiles</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                                <div class="tab-pane" id="sender" role="tabpanel">

                                    <div class="row row-button tab-button">
                                        <div class="col-md-12">
                                            <a type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#addSender"><i class="bx bx-user-plus font-size-20 align-middle me-2"></i> Add Sender</a>
                                        </div>
                                    </div>

                                    <div class="loader-table" style="padding-bottom: 30px;">
                                        <img src="<?= base_url("public/assets/images/loader.gif") ?>" alt="spinner" width="80" style="position: absolute; padding-top: 88px; margin-left: 40%;">
                                    </div>

                                    <table id="dataSender" class="table table-bordered dt-responsive  nowrap w-100">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Cp Name</th>
                                                <th>Sender</th>
                                                <th>SID</th>
                                                <th>Type</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/clients'); ?>
<?= view('modal/clients'); ?>

<script>
    GetDetail('<?= $clientId ?>');
    GetSender();
    GetConfigProfile();
</script>