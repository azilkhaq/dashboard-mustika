<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Config Profile</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Config Profile</li>
                                <li class="breadcrumb-item">Add Config Profile</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Form Add Config Profile</h4>

                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Name</label>
                                            <input type="text" class="form-control" id="name" placeholder="Enter name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Data Center</label>
                                            <input type="text" class="form-control" id="dataCenter" placeholder="Enter data center">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>DB Host</label>
                                            <input type="text" class="form-control" id="dbHost" placeholder="Enter dbHost">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>DB Name</label>
                                            <input type="text" class="form-control" id="dbName" placeholder="Enter dbName">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Table Name</label>
                                            <input type="text" class="form-control" id="tableName" placeholder="Enter table name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Username</label>
                                            <input type="text" class="form-control" id="username" placeholder="Enter username">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Remark</label>
                                            <input type="text" class="form-control" id="remark" placeholder="Enter remark">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Password</label>
                                            <input type="text" class="form-control" id="password" placeholder="Enter password">
                                        </div>
                                    </div>
                                </div>

                                <div class="button-form">
                                    <a href="<?= base_url('configProfile') ?>" class="btn btn-danger w-md"><i class="bx bx-x-circle font-size-20 align-middle me-2"></i> Batal</a>&nbsp;
                                    <button type="button" class="btn btn-primary w-md" onclick="Create()" id="create"><i class="bx bx-send font-size-20 align-middle me-2"></i> Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/configProfile'); ?>