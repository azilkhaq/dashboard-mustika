<script>
    ChangePassword = async () => {
        let data = new FormData();
        var oldPassword = $("#oldPassword").val();
        var newPassword = $("#newPassword").val();
        var confirmPassword = $("#confirmPassword").val();

        data.append('oldPassword', oldPassword);
        data.append('newPassword', newPassword);
        data.append('confirmPassword', confirmPassword);

        if (newPassword != confirmPassword) {
            Swal.fire({
                title: "Validation",
                text: "Confirm password does not match!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        $("#changePassword").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#changePassword").prop("disabled", true);

        await $.ajax({
            type: "POST",
            url: `${baseUrl}/profile/change-password`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Change password successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/profile`;
                }, 2000);
            },
            error: function(err) {
                $("#changePassword").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#changePassword").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Unable to change password, please make sure you entered valid password",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

   
    
</script>