<script>
    var table = $('#data').DataTable({
        scrollX: true
    });

    var tablePrefix = $('#dataPrefix').DataTable({
        scrollX: true
    });

    var tableConfig = $('#dataConfig').DataTable();

    var tableSender = $('#dataSender').DataTable();

    async function Data() {

        table.clear().draw();

        $(".odd").css({
            "display": "none"
        });
        $(".loader-table").css({
            "display": "block"
        });

        await $.ajax({
            type: "POST",
            url: `${baseUrl}/clients/getData`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;

                var dataRows = res.body.map(element => {

                    return [
                        no++,
                        element.clientId ? element.clientId : "-",
                        element.user.fullname ? element.user.fullname : "-",
                        element.user.email ? element.user.email : "-",
                        element.created ? moment(element.created).format('YYYY-MM-DD HH:mm:ss') : "-",
                        `<a href="${baseUrl}/clients/edit/${element.clientId}" class="btn btn-warning btn-sm waves-effect waves-light"><i class="bx bx-edit-alt font-size-16 align-middle me-2"></i> Edit</a>&nbsp;
                         <a href="${baseUrl}/clients/detail/${element.clientId}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="bx bx-info-circle font-size-16 align-middle me-2"></i> Detail</a>&nbsp;
                         <a type="button" onclick="Delete('${element.clientId}')" class="btn btn-danger btn-sm waves-effect waves-light"><i class="bx bxs-trash-alt font-size-16 align-middle me-2"></i>Delete</a>`
                    ]
                });

                table.rows.add(dataRows).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
                console.log(err);
            }
        });
    }

    function Delete(Id) {

        let data = new FormData();

        data.append('Id', Id);

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes, delete it!"
        }).then(function(t) {

            t.value ? $.ajax({
                type: "POST",
                url: `${baseUrl}/clients/delete`,
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                success: function(response) {

                    Swal.fire("Successfuly!", "clients has been deleted.", "success")
                    Data();
                },
                error: function(err) {

                    $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                    $("#create").prop("disabled", false);

                    Swal.fire("Error!", "Something went wrong, please try again.", "error")
                }
            }) : t.dismiss === Swal.DismissReason.cancel;
        });
    }

    Create = () => {
        let data = new FormData();

        var fullname = $("#fullname").val();
        var email = $("#email").val();
        var userId = $("#userId").val();
        var password = $("#password").val();
        let profilePic = $('#image')[0].files[0];
        var role = $("#role").val();
        var confirmPassword = $("#confirmPassword").val();
        var senderId = $("#senderId").val();
        var configProfileNames = $("#configProfileNames").val();

        if (userId == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty userId!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var re = /\s/g;
        if (re.test(userId)) {
            Swal.fire({
                title: "Validation",
                text: "userId must not contain spaces!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        let lengthUserId = userId.length;

        if (lengthUserId < 6) {
            Swal.fire({
                title: "Validation",
                text: "userId size must be between 6 and 50!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (fullname == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty fullname!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (email == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty email!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!email.match(mailformat)) {
            Swal.fire({
                title: "Validation",
                text: "Invalid email!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (role == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty role!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (password == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty password!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (password != confirmPassword) {
            Swal.fire({
                title: "Validation",
                text: "Confirm password does not match!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (senderId == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty senderId!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }


        if (configProfileNames == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty configProfileNames!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        listPrefixes = [];

        $('.item-prefixes').each(function(e) {
            var prefix = $("input[name='outer-group[0][inner-group][" + e + "][prefix]']").val()
            var type = $("input[name='outer-group[0][inner-group][" + e + "][type]']").val()
            var pattern = $("input[name='outer-group[0][inner-group][" + e + "][pattern]']").val()
            var remark = $("input[name='outer-group[0][inner-group][" + e + "][remark]']").val()

            listPrefixes.push({
                prefix: prefix,
                type: type,
                pattern: pattern,
                remark: remark
            });
        });

        data.append('fullname', fullname);
        data.append('email', email);
        data.append('userId', userId);
        data.append('password', password);
        data.append('profilePic', profilePic);
        data.append('role', role);
        data.append('configProfileNames', configProfileNames);
        data.append('senderId', senderId);
        data.append('prefixes', JSON.stringify(listPrefixes));

        $("#create").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#create").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/clients/postCreate`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Add clients successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/clients`;
                }, 2000);
            },
            error: function(err) {

                $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#create").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

    GetSender = () => {
        $.ajax({
            type: "POST",
            url: `${baseUrl}/sender/getData`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                var res = JSON.parse(response);

                $('#senderId').find('option').remove().end();
                $("#senderId").removeAttr("disabled")

                res.body.map(element => {

                    $('#senderId').append($('<option>', {
                        value: element.id,
                        text: element.sender
                    }));
                });
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    GetConfigProfile = () => {
        $.ajax({
            type: "POST",
            url: `${baseUrl}/configProfile/getData`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                var res = JSON.parse(response);

                $('#configProfileNames').find('option').remove().end();
                $("#configProfileNames").removeAttr("disabled")

                res.body.map(element => {

                    $('#configProfileNames').append($('<option>', {
                        value: element.name,
                        text: element.name
                    }));
                });
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    Edit = () => {
        let data = new FormData();

        var clientId = $("#clientId").val();
        var fullname = $("#fullname").val();
        var email = $("#email").val();
        var userId = $("#userId").val();
        var password = $("#password").val();
        let profilePic = $('#image')[0].files[0];
        var role = $("#role").val();
        var confirmPassword = $("#confirmPassword").val();
        var senderId = $("#senderId").val();
        var configProfileNames = $("#configProfileNames").val();

        if (userId == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty userId!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var re = /\s/g;
        if (re.test(userId)) {
            Swal.fire({
                title: "Validation",
                text: "userId must not contain spaces!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        let lengthUserId = userId.length;

        if (lengthUserId < 6) {
            Swal.fire({
                title: "Validation",
                text: "userId size must be between 6 and 50!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (fullname == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty fullname!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (email == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty email!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!email.match(mailformat)) {
            Swal.fire({
                title: "Validation",
                text: "Invalid email!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (role == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty role!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (senderId == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty senderId!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }


        if (configProfileNames == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty configProfileNames!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        listPrefixes = [];

        $('.item-prefixes').each(function(e) {
            var prefix = $("input[name='outer-group[0][inner-group][" + e + "][prefix]']").val()
            var type = $("input[name='outer-group[0][inner-group][" + e + "][type]']").val()
            var pattern = $("input[name='outer-group[0][inner-group][" + e + "][pattern]']").val()
            var remark = $("input[name='outer-group[0][inner-group][" + e + "][remark]']").val()

            listPrefixes.push({
                prefix: prefix,
                type: type,
                pattern: pattern,
                remark: remark
            });
        });

        data.append('clientId', clientId);
        data.append('fullname', fullname);
        data.append('email', email);
        data.append('userId', userId);
        data.append('password', password);
        data.append('profilePic', profilePic);
        data.append('role', role);
        data.append('configProfileNames', configProfileNames);
        data.append('senderId', senderId);
        data.append('prefixes', JSON.stringify(listPrefixes));

        $("#edit").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#edit").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/clients/postEdit`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Edit clients successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/clients`;
                }, 2000);
            },
            error: function(err) {

                $("#edit").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#edit").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

    GetDetail = (clientId) => {
        tablePrefix.clear().draw();
        tableConfig.clear().draw();
        tableSender.clear().draw();

        $(".odd").css({
            "display": "none"
        });
        $(".loader-table").css({
            "display": "block"
        });

        $.ajax({
            type: "POST",
            url: `${baseUrl}/clients/getDetail/${clientId}`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;
                var configNo = 1;
                var senderNo = 1;

                var dataPrefix = res.body.prefixes.map(element => {
                    return [
                        no++,
                        element.prefix ? element.prefix : "-",
                        element.type ? element.type : "-",
                        element.pattern ? element.pattern : "-",
                        element.remark ? element.remark : "-"
                    ]
                });

                var dataConfig = res.body.configProfiles.map(element => {
                    return [
                        configNo++,
                        element ? element : "-",
                        `<a type="button" onclick="DeleteProfile('${res.body.clientId}', '${element}')" class="btn btn-danger btn-sm waves-effect waves-light"><i class="bx bxs-trash-alt font-size-16 align-middle me-2"></i>Delete</a>`
                    ]
                });

                var dataSender = res.body.senders.map(element => {
                    return [
                        senderNo++,
                        element.cpName ? element.cpName : "-",
                        element.sender ? element.sender : "-",
                        element.sid ? element.sid : "-",
                        element.type ? element.type : "-",
                        `<a type="button" onclick="DeleteSender('${res.body.clientId}', '${element.id}')" class="btn btn-danger btn-sm waves-effect waves-light"><i class="bx bxs-trash-alt font-size-16 align-middle me-2"></i>Delete</a>`
                    ]
                });

                tablePrefix.rows.add(dataPrefix).draw();
                tableConfig.rows.add(dataConfig).draw();
                tableSender.rows.add(dataSender).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
            }
        });
    }

    DeleteSender = (clientId, senderId) => {

        let data = new FormData();

        data.append('clientId', clientId);
        data.append('senderId', senderId);

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes, delete it!"
        }).then(function(t) {

            t.value ? $.ajax({
                type: "POST",
                url: `${baseUrl}/clients/deleteSender`,
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                success: function(response) {

                    Swal.fire("Successfuly!", "sender has been deleted.", "success")
                    GetDetail(clientId);
                },
                error: function(err) {

                    $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                    $("#create").prop("disabled", false);

                    Swal.fire("Error!", "Something went wrong, please try again.", "error")
                }
            }) : t.dismiss === Swal.DismissReason.cancel;
        });
    }


    DeleteProfile = (clientId, configProfileNames) => {

        let data = new FormData();

        data.append('clientId', clientId);
        data.append('configProfileNames', configProfileNames);

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes, delete it!"
        }).then(function(t) {

            t.value ? $.ajax({
                type: "POST",
                url: `${baseUrl}/clients/deleteProfile`,
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                success: function(response) {

                    Swal.fire("Successfuly!", "config profile has been deleted.", "success")
                    GetDetail(clientId);
                },
                error: function(err) {

                    $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                    $("#create").prop("disabled", false);

                    Swal.fire("Error!", "Something went wrong, please try again.", "error")
                }
            }) : t.dismiss === Swal.DismissReason.cancel;
        });
    }

    AddSender = (clientId) => {
        let data = new FormData();

        var senderId = $("#senderId").val();

        if (senderId == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty senderId!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        data.append('clientId', clientId);
        data.append('senderId', senderId);

        $("#add").text('Loading...');
        $("#add").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/clients/postAddSender`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Add Sender successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                });

                $("#addSender").modal("hide");

                GetDetail(clientId);
            },
            error: function(err) {

                $("#add").text('Submit');
                $("#add").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

    AddProfile = (clientId) => {
        let data = new FormData();

        var configProfileNames = $("#configProfileNames").val();

        if (configProfileNames == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty configProfileNames!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        data.append('clientId', clientId);
        data.append('configProfileNames', configProfileNames);

        $("#add").text('Loading...');
        $("#add").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/clients/postAddProfile`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Add Config Profile successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                });

                $("#addProfile").modal("hide");

                GetDetail(clientId);
            },
            error: function(err) {

                $("#add").text('Submit');
                $("#add").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }
</script>