<script>
    $(".null-data").text("No data available");

    function Data(start, end, filter, indexs) {

        $(".null-data").text("");

        $(".loader-table").css({
            "display": "block"
        });

        $('#myChart').remove();
        $('.chart-div').append('<canvas id="myChart"></canvas>');

        let data = new FormData();

        data.append('indexs', indexs);
        data.append('filter', filter);
        data.append('start', start);
        data.append('end', end);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/report/getDataTrafficTime`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                var res = JSON.parse(response);

                var data = res.body;

                labels = [];
                datasetsZero = [];
                datasetsOne = [];
                datasetsTwo = [];
                datasetsThre = [];
                datasetsFour = [];

                data.forEach(element => {
                    labels.push(element.key);

                    var valueZero = 0;
                    var valueOne = 0;
                    var valueTwo = 0;
                    var valueThre = 0;
                    var valueFour = 0;

                    var buckets = element.buckets;

                    function valExists(key) {
                        return buckets.some(function(el) {
                            return el.key === key;
                        });
                    }

                    let objZero = buckets.find(o => o.key === "0");

                    if (objZero == undefined) {
                        valueZero = 0;
                    } else {
                        valueZero = objZero.value;
                    }

                    let objOne = buckets.find(o => o.key === "1");

                    if (objOne == undefined) {
                        valueOne = 0;
                    } else {
                        valueOne = objOne.value;
                    }

                    let objTwo = buckets.find(o => o.key === "5:667");

                    if (objTwo == undefined) {
                        valueTwo = 0;
                    } else {
                        valueTwo = objTwo.value;
                    }

                    let objThre = buckets.find(o => o.key === "5:668");

                    if (objThre == undefined) {
                        valueThre = 0;
                    } else {
                        valueThre = objThre.value;
                    }

                    let objFour = buckets.find(o => o.key === "N/A");

                    if (objFour == undefined) {
                        valueFour = 0;
                    } else {
                        valueFour = objFour.value;
                    }

                    datasetsZero.push(valueZero);
                    datasetsOne.push(valueOne);
                    datasetsTwo.push(valueTwo);
                    datasetsThre.push(valueThre);
                    datasetsFour.push(valueFour);
                });

                var ctx = document.getElementById("myChart").getContext("2d");

                var data = {
                    labels: labels,
                    datasets: [{
                        label: "0",
                        backgroundColor: "#10A210",
                        data: datasetsZero
                    }, {
                        label: "1",
                        backgroundColor: "#0465DF",
                        data: datasetsOne
                    }, {
                        label: "5:667",
                        backgroundColor: "#F8D300",
                        data: datasetsTwo
                    }, {
                        label: "5:668",
                        backgroundColor: "#F76010",
                        data: datasetsThre
                    }, {
                        label: "N/A",
                        backgroundColor: "#D5082F",
                        data: datasetsFour
                    }]
                };

                config = {
                    type: 'bar',
                    data: data,
                    options: {
                        barValueSpacing: 20,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: 0,
                                }
                            }]
                        }
                    }
                };

                $(".loader-table").css({
                    "display": "none"
                });

                var myBarChart = new Chart(ctx, config);
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
                $(".null-data").text("No data available");
            }
        });
    }

    function Submit() {

        var indexs = $("#indexs").val();
        var start = $("#start").val();
        var end = $("#end").val();
        var filter = $('input[name="filter"]:checked').val();

        var startTime = moment(start).format('YYYY-MM-DD HH:mm:ss');
        var endTime = moment(end).format('YYYY-MM-DD HH:mm:ss');

        const finalStart = new Date(startTime)
        const finalEnd = new Date(endTime)

        let hours = Math.abs(finalStart - finalEnd) / 36e5;

        if (start == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty starttime!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (end == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty endtime!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (filter == undefined) {
            Swal.fire({
                title: "Validation",
                text: "Checked Per Hours or Per Days!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        } else {
            if (filter == "hours") {
                if (hours > 24) {
                    Swal.fire({
                        title: "Validation",
                        text: "The selected date must be 24 hours!",
                        icon: "error",
                        confirmButtonColor: "#556ee6"
                    })
                    return
                }

                if (hours < 1) {
                    Swal.fire({
                        title: "Validation",
                        text: "The selected date should not be less than 1 hour!",
                        icon: "error",
                        confirmButtonColor: "#556ee6"
                    })
                    return
                }

            }
        }

        if (filter == "hours") {
            filter = "h";
        } else {
            filter = "d";
        }

        Data(start, end, filter, indexs);
    }
</script>