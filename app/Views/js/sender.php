<script>
    var table = $('#data').DataTable({
        scrollX: true,
    });

    async function Data() {

        table.clear().draw();

        $(".odd").css({
            "display": "none"
        });
        $(".loader-table").css({
            "display": "block"
        });

        await $.ajax({
            type: "POST",
            url: `${baseUrl}/sender/getData`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;

                var dataRows = res.body.map(element => {

                    return [
                        no++,
                        `<a href="${baseUrl}/sender/edit/${element.id}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="bx bx-edit-alt font-size-16 align-middle me-2"></i>Edit</a>&nbsp;
                        <a type="button" onclick="Delete('${element.id}')" class="btn btn-danger btn-sm waves-effect waves-light"><i class="bx bxs-trash-alt font-size-16 align-middle me-2"></i>Delete</a>`,
                        element.cpName ? element.cpName : "-",
                        element.sender ? element.sender : "-",
                        element.sid ? element.sid : "-",
                        element.pwd ? element.pwd : "-",
                        element.owner ? element.owner : "-",
                        element.type ? element.type : "-",
                        element.url ? element.url : "-"
                    ]
                });

                table.rows.add(dataRows).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
                console.log(err);
            }
        });
    }

    Data();

    function Delete(Id) {

        let data = new FormData();

        data.append('Id', Id);

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes, delete it!"
        }).then(function(t) {

            t.value ? $.ajax({
                type: "POST",
                url: `${baseUrl}/sender/delete`,
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                success: function(response) {

                    Swal.fire("Successfuly!", "sender has been deleted.", "success")
                    Data();
                },
                error: function(err) {

                    $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                    $("#create").prop("disabled", false);

                    Swal.fire("Error!", "Something went wrong, please try again.", "error")
                }
            }) : t.dismiss === Swal.DismissReason.cancel;
        });
    }

    $(".swal2-cancel").click(function() {
        alert("kons");
    });

    Create = () => {
        let data = new FormData();

        var cpName = $("#cpName").val();
        var sender = $("#sender").val();
        var sid = $("#sid").val();
        var url = $("#url").val();
        let pwd = $('#pwd').val();
        var owner = $("#owner").val();
        var type = $("#type").val();

        if (cpName == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty cpName!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (sender == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty sender!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (sid == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty sid!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (url == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty url!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (pwd == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty pwd!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (owner == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty owner!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (type == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty type!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        data.append('cpName', cpName);
        data.append('sender', sender);
        data.append('sid', sid);
        data.append('url', url);
        data.append('pwd', pwd);
        data.append('owner', owner);
        data.append('type', type);

        $("#create").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#create").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/sender/postCreate`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Add sender successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/sender`;
                }, 2000);
            },
            error: function(err) {

                $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#create").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

    Edit = () => {
        let data = new FormData();

        var Id = $("#Id").val();
        var cpName = $("#cpName").val();
        var sender = $("#sender").val();
        var sid = $("#sid").val();
        var url = $("#url").val();
        let pwd = $('#pwd').val();
        var owner = $("#owner").val();
        var type = $("#type").val();

        if (cpName == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty cpName!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (sender == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty sender!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (sid == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty sid!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (url == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty url!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (pwd == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty pwd!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (owner == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty owner!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (type == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty type!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        data.append('Id', Id);
        data.append('cpName', cpName);
        data.append('sender', sender);
        data.append('sid', sid);
        data.append('url', url);
        data.append('pwd', pwd);
        data.append('owner', owner);
        data.append('type', type);

        $("#edit").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#edit").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/sender/postEdit`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Edit sender successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/sender`;
                }, 2000);
            },
            error: function(err) {

                $("#edit").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#edit").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }
</script>