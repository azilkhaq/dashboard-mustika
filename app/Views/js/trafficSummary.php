<script>
    var table = $('#data').DataTable({
        scrollX: true,
        lengthChange: false,
        searching: false,
        "aoColumnDefs": [{
            "sClass": "average",
            "aTargets": [3, 4, 5, 6, 7]
        }]
    });

    function Data(indexs, start, end, prefix, sid) {

        table.clear().draw({
            scrollX: true,
            lengthChange: false,
            searching: false
        });

        $(".odd").css({
            "display": "none"
        });

        $(".loader-table").css({
            "display": "block"
        });

        let data = new FormData();

        data.append('indexs', indexs);
        data.append('start', start);
        data.append('end', end);
        data.append('prefix', prefix);
        data.append('sid', sid);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/report/getDataTrafficSummary`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;
                var prefix = res.body[0].key;

                var dataRows = res.body[0].buckets.map(element => {

                    successCount = [];
                    successSum = [];

                    failedCount = [];
                    failedSum = [];

                    element.buckets.map(value => {

                        if (value.key == "1") {
                            successCount.push(value.value);
                            successSum.push(value.buckets[0].value);
                        } else {
                            failedCount.push(value.value);
                            failedSum.push(value.buckets[0].value);
                        }

                    });

                    return [
                        no++,
                        prefix,
                        element.key ? element.key : "-",
                        element.value ? element.value : "-",
                        successCount.reduce((a, b) => a + b, 0),
                        successSum.reduce((a, b) => a + b, 0),
                        failedCount.reduce((a, b) => a + b, 0),
                        failedSum.reduce((a, b) => a + b, 0),
                    ]
                });

                table.rows.add(dataRows).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
            }
        });
    }

    function Submit() {

        var indexs = $("#indexs").val();
        var start = $("#start").val();
        var end = $("#end").val();
        var prefix = $("#prefix").val();
        var sid = $("#sid").val();

        if (sRole != "PARTNER") {

            if (indexs == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty indexs!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

        }

        if (start == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty start!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (end == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty end!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (sRole != "PARTNER") {

            if (prefix == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty prefix!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

        }

        Data(indexs, start, end, prefix, sid);
    }

    $("#indexs").change(function() {

        var data = new FormData();
        var value = $(this).val();

        data.append('value', value);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/report/getPrefix`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                var res = JSON.parse(response);

                $('#prefix').find('option').remove().end();
                $("#prefix").removeAttr("disabled")

                $('#prefix').append($('<option>', {
                    value: "",
                    text: "Select",
                }));

                var i = 0;

                res.body.map(element => {

                    element.prefixes.map(value => {

                        i++;

                        if (i > 1) {
                            if (value == "*") {
                                value = "duplicate";
                            }
                        }

                        var all = "";

                        if (value == "*") {
                            all = "(ALL)";
                        }

                        if (value != "duplicate") {

                            $('#prefix').append($('<option>', {
                                value: value,
                                text: value + all,
                            }));
                        }
                    });
                });
            },
            error: function(err) {
                console.log(err);
            }
        });
    });
</script>