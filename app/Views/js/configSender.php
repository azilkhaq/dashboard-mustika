<script>

    var table = $('#data').DataTable({
        scrollX: true
    });

    function Data(name) {

        table.clear().draw({
            scrollX: true
        });

        $(".odd").css({
            "display": "none"
        });
        $(".loader-table").css({
            "display": "block"
        });

        $.ajax({
            type: "POST",
            url: `${baseUrl}/configSender/getData/${name}`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;

                var dataRows = res.body.map(element => {

                    return [
                        no++,
                        element.profileName ? element.profileName : "-",
                        element.cpName ? element.cpName : "-",
                        element.sender ? element.sender : "-",
                        element.sid ? element.sid : "-",
                        element.url ? element.url : "-",
                        element.pwd ? element.pwd : "-",
                        element.owner ? element.owner : "-",
                        element.type ? element.type : "-",
                    ]
                });

                table.rows.add(dataRows).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
            }
        });
    }

    function Submit() {

        var name = $("#name").val();

        Data(name);
    }

    function SubmitForm() {

        var name = $("#name").val();

        if (name == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty profile!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        $("#profileName").val(name);

        $(".form-sender").css("display", "block");
    }

    Create = () => {
        let data = new FormData();

        var profileName = $("#profileName").val();
        var cpName = $("#cpName").val();
        var sender = $("#sender").val();
        var sid = $("#sid").val();
        var url = $("#url").val();
        var pwd = $("#pwd").val();
        var owner = $("#owner").val();
        var type = $("#type").val();

        if (profileName == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty profile name!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (cpName == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty cpName!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (sender == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty sender!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (sid == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty sid!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (url == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty url!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (pwd == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty pwd!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (owner == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty owner!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (type == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty type!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        data.append('profileName', profileName);
        data.append('cpName', cpName);
        data.append('sender', sender);
        data.append('sid', sid);
        data.append('url', url);
        data.append('pwd', pwd);
        data.append('owner', owner);
        data.append('type', type);

        $("#create").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#create").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/configSender/postCreate`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Add config sender successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/configSender`;
                }, 2000);
            },
            error: function(err) {

                $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#create").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }
</script>