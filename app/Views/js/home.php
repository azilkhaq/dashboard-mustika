<script>
    $(".total-transaction").text("Loading...");
    $(".total-sms").text("Loading...");

    async function Data() {
        await $.ajax({
            type: "POST",
            url: `${baseUrl}/home/getData`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                var res = JSON.parse(response);

                $(".total-transaction").text(numberWithCommas(res.count));
                $(".total-sms").text(numberWithCommas(res.sum));

            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    Data();

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
</script>