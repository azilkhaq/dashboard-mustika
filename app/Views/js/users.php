<script>
    var table = $('#data').DataTable();

    async function Data() {

        table.clear().draw();

        $(".odd").css({
            "display": "none"
        });
        $(".loader-table").css({
            "display": "block"
        });

        await $.ajax({
            type: "POST",
            url: `${baseUrl}/users/getData`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;

                var dataRows = res.body.map(element => {

                    return [
                        no++,
                        element.userId ? element.userId : "-",
                        element.fullname ? element.fullname : "-",
                        element.email ? element.email : "-",
                        element.role ? element.role : "-",
                        `<a href="${baseUrl}/users/edit/${element.userId}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="bx bx-edit-alt font-size-16 align-middle me-2"></i> Edit</a>&nbsp;
                         <a type="button" onclick="Detail('${element.userId}')" id="${element.userId}" class="btn btn-warning btn-sm waves-effect waves-light"><i class="bx bx-image font-size-16 align-middle me-2"></i> View Profile</a>`
                    ]
                });

                table.rows.add(dataRows).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
                console.log(err);
            }
        });
    }

    Data();

    Create = () => {
        let data = new FormData();

        var fullname = $("#fullname").val();
        var email = $("#email").val();
        var username = $("#username").val();
        var password = $("#password").val();
        let profilePic = $('#image')[0].files[0];
        var role = $("#role").val();
        var confirmPassword = $("#confirmPassword").val();
        var clientId = $("#clientId").val();
        var prefix = $("#prefix").val();
        var typePartner = $("#typePartner").val();
        var pattern = $("#pattern").val();
        var remark = $("#remark").val();

        if (username == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty username!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var re = /\s/g;
        if (re.test(username)) {
            Swal.fire({
                title: "Validation",
                text: "Username must not contain spaces!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        let lengthUsername = username.length;

        if (lengthUsername < 6) {
            Swal.fire({
                title: "Validation",
                text: "Username size must be between 6 and 50!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (fullname == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty fullname!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (email == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty email!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!email.match(mailformat)) {
            Swal.fire({
                title: "Validation",
                text: "Invalid email!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (role == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty role!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (role == "PARTNER") {

            if (clientId == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty clientId!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

            if (prefix == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty prefix!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

            if (typePartner == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty type!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

            if (pattern == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty pattern!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

        }


        if (password == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty password!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (password != confirmPassword) {
            Swal.fire({
                title: "Validation",
                text: "Confirm password does not match!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        data.append('fullname', fullname);
        data.append('email', email);
        data.append('username', username);
        data.append('password', password);
        data.append('profilePic', profilePic);
        data.append('role', role);
        data.append('clientId', clientId);
        data.append('prefix', prefix);
        data.append('typePartner', typePartner);
        data.append('pattern', pattern);
        data.append('remark', remark);

        $("#create").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#create").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/users/postCreate`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Add users successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/users`;
                }, 2000);
            },
            error: function(err) {

                $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#create").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

    Edit = () => {
        let data = new FormData();
        var userId = $("#userId").val();
        var fullname = $("#fullname").val();
        var email = $("#email").val();
        var username = $("#username").val();
        var password = $("#password").val();
        let profilePic = $('#image')[0].files[0];
        var confirmPassword = $("#confirmPassword").val();
        var valProfilePic = $("#valProfilePic").val();
        var clientId = $("#clientId").val();
        var prefix = $("#prefix").val();
        var typePartner = $("#typePartner").val();
        var pattern = $("#pattern").val();
        var remark = $("#remark").val();
        var role = $("#role").val();

        if (username == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty username!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var re = /\s/g;
        if (re.test(username)) {
            Swal.fire({
                title: "Validation",
                text: "Username must not contain spaces!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        let lengthUsername = username.length;

        if (lengthUsername < 6) {
            Swal.fire({
                title: "Validation",
                text: "Username size must be between 6 and 50!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (fullname == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty fullname!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (email == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty email!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (role == "PARTNER") {
            if (clientId == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty clientId!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

            if (prefix == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty prefix!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

            if (typePartner == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty type!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

            if (pattern == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty pattern!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }
        }

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!email.match(mailformat)) {
            Swal.fire({
                title: "Validation",
                text: "Invalid email!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (password != "") {

            if (password != confirmPassword) {
                Swal.fire({
                    title: "Validation",
                    text: "Confirm password does not match!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }

        }

        data.append('userId', userId);
        data.append('fullname', fullname);
        data.append('email', email);
        data.append('username', username);
        data.append('password', password);
        data.append('profilePic', profilePic);
        data.append('valProfilePic', valProfilePic);
        data.append('clientId', clientId);
        data.append('prefix', prefix);
        data.append('typePartner', typePartner);
        data.append('pattern', pattern);
        data.append('remark', remark);
        data.append('role', role);

        $("#edit").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#edit").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/users/postEdit`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Edit users successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/users`;
                }, 2000);
            },
            error: function(err) {

                $("#edit").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#edit").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

    Detail = (userId) => {

        $("#" + userId).html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#" + userId).prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/users/getDetail/${userId}`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                $("#" + userId).html('<i class="bx bx-image font-size-16 align-middle me-2"></i> View Profile');
                $("#" + userId).prop("disabled", false);

                var res = JSON.parse(response);

                $("#profileImage").modal("show");
                $("#imageProfile").attr("src", mediaUrl + res.body.profilePic);
            },
            error: function(err) {

                $("#" + userId).html('<i class="bx bx-image font-size-16 align-middle me-2"></i> View Profile');
                $("#" + userId).prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

    $("#role").change(function() {
        var role = this.value;

        if (role == "PARTNER") {
            $(".partner").css("display", "block");
        } else {
            $(".partner").css("display", "none");
        }
    });
</script>