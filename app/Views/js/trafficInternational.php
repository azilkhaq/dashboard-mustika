<script>
    var table = $('#data').DataTable({
        lengthChange: false,
        searching: false,
        "aoColumnDefs": [{
            "sClass": "average",
            "aTargets": [4]
        }]
    });

    function Data(start, end, client, sender) {

        table.clear().draw({
            scrollX: true,
            lengthChange: false,
            searching: false
        });

        $(".odd").css({
            "display": "none"
        });

        $(".loader-table").css({
            "display": "block"
        });

        let data = new FormData();

        data.append('start', start);
        data.append('end', end);
        data.append('client', client);
        data.append('sender', sender);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/report/getDataTrafficInternational`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;

                var dataRows = res.body.map(element => {
                    return [
                        no++,
                        element.name ? element.name : "-",
                        element.sender ? element.sender : "-",
                        element.status ? element.status : "-",
                        element.value ? numberWithCommas(element.value) : "-",
                    ]
                });

                table.rows.add(dataRows).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
            }
        });
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function Submit() {

        var start = $("#start").val();
        var end = $("#end").val();
        var client = $("#client").val();
        var sender = $("#sender").val();

        if (start == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty start!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (end == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty end!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        Data(start, end, client, sender);
    }

    var start = $("#start").val();
    var end = $("#end").val();

    Data(start, end, "", "");
</script>