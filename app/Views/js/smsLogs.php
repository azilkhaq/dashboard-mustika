<script>
    var table = $('#dataSmsLogs').DataTable({
        scrollX: true,
        lengthChange: false,
        searching: false
    });

    function Data(indexs, start, end, search, msisdn, sid, cpname, trxid, prefix) {

        table.clear().draw({
            scrollX: true,
            lengthChange: false,
            searching: false
        });

        $(".odd").css({
            "display": "none"
        });

        $(".loader-table").css({
            "display": "block"
        });

        let data = new FormData();

        data.append('indexs', indexs);
        data.append('start', start);
        data.append('end', end);
        data.append('search', search);
        data.append('msisdn', msisdn);
        data.append('sid', sid);
        data.append('cpname', cpname);
        data.append('trxid', trxid);
        data.append('prefix', prefix);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/report/getDataSmsLogs`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;

                var dataRows = res.body.map(element => {

                    return [
                        no++,
                        element.msisdn ? element.msisdn : "-",
                        element.response ? element.response : "-",
                        element.year ? element.year : "-",
                        element.month ? element.month : "-",
                        element.date ? element.date : "-",
                        element.time ? element.time : "-",
                        element.timestamp ? element.timestamp : "-",
                        element.smsCount ? element.smsCount : "-",
                        element.trxid ? element.trxid : "-",
                        element.cpname ? element.cpname : "-",
                        element.proc ? element.proc : "-",
                        element.path ? element.path : "-",
                        element.url ? element.url : "-",
                        element.host ? element.host : "-",
                        element.sid ? element.sid : "-",
                        element.message ? element.message : "-",
                        element.sms ? element.sms : "-",
                    ]
                });

                table.rows.add(dataRows).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
            }
        });
    }

    function Submit() {

        var indexs = $("#indexs").val();
        var start = $("#start").val();
        var end = $("#end").val();
        var search = $("#search").val();
        var msisdn = $("#msisdn").val();
        var sid = $("#sid").val();
        var cpname = $("#cpname").val();
        var trxid = $("#trxid").val();
        var prefix = $("#prefix").val();

        if (sRole != "PARTNER") {
            if (indexs == "") {
                Swal.fire({
                    title: "Validation",
                    text: "Empty indexs!",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
                return
            }
        }

        if (start == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty start!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (end == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty end!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        Data(indexs, start, end, search, msisdn, sid, cpname, trxid, prefix);
    }

    $("#indexs").change(function() {

        var data = new FormData();
        var value = $(this).val();

        data.append('value', value);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/report/getPrefix`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                var res = JSON.parse(response);

                $('#prefix').find('option').remove().end();
                $("#prefix").removeAttr("disabled")

                $('#prefix').append($('<option>', {
                    value: "",
                    text: "Select",
                }));

                var i = 0;

                res.body.map(element => {

                    element.prefixes.map(value => {

                        i++;

                        if (i > 1) {
                            if (value == "*") {
                                value = "duplicate";
                            }
                        }

                        var all = "";

                        if (value == "*") {
                            all = "(ALL)";
                        }

                        if (value != "duplicate") {

                            $('#prefix').append($('<option>', {
                                value: value,
                                text: value + all,
                            }));
                        }
                    });
                });
            },
            error: function(err) {
                console.log(err);
            }
        });
    });
</script>