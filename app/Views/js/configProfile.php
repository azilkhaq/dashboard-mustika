<script>
    var table = $('#data').DataTable({
        scrollX: true,
    });

    async function Data() {

        table.clear().draw({
            scrollX: true
        });

        $(".odd").css({
            "display": "none"
        });
        $(".loader-table").css({
            "display": "block"
        });

        await $.ajax({
            type: "POST",
            url: `${baseUrl}/configProfile/getData`,
            cache: false,
            contentType: false,
            processData: false,
            data: false,
            success: function(response) {

                $(".loader-table").css({
                    "display": "none"
                });

                var res = JSON.parse(response);

                if (res.body == "") {
                    $(".odd").css({
                        "display": "table-row"
                    });
                }

                var no = 1;

                var dataRows = res.body.map(element => {

                    return [
                        no++,
                        element.name ? element.name : "-",
                        element.dbHost ? element.dbHost : "-",
                        element.dbName ? element.dbName : "-",
                        element.dataCenter ? element.dataCenter : "-",
                        element.tableName ? element.tableName : "-",
                        element.username ? element.username : "-",
                        element.password ? element.password : "-",
                        element.sharding,
                        element.remark ? element.remark : "-",
                        element.connected,
                        element.created ? element.created : "-",
                        `<a href="${baseUrl}/configProfile/edit/${element.name}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="bx bx-edit-alt font-size-16 align-middle me-2"></i> Edit</a>&nbsp;`
                    ]
                });

                table.rows.add(dataRows).draw();
            },
            error: function(err) {
                $(".loader-table").css({
                    "display": "none"
                });
                console.log(err);
            }
        });
    }

    Data();

    Create = () => {
        let data = new FormData();

        var name = $("#name").val();
        var dataCenter = $("#dataCenter").val();
        var dbHost = $("#dbHost").val();
        var dbName = $("#dbName").val();
        var tableName = $("#tableName").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var remark = $("#remark").val();

        if (name == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty name!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var re = /\s/g;
        if (re.test(name)) {
            Swal.fire({
                title: "Validation",
                text: "Username must not contain spaces!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (dataCenter == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty data center!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (dbHost == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty dbHost!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (dbName == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty dbName!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (tableName == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty tableName!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (username == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty username!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (remark == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty remark!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (password == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty password!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        data.append('name', name);
        data.append('dataCenter', dataCenter);
        data.append('dbHost', dbHost);
        data.append('dbName', dbName);
        data.append('tableName', tableName);
        data.append('username', username);
        data.append('password', password);
        data.append('remark', remark);

        $("#create").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#create").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/configProfile/postCreate`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Add config profile successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/configProfile`;
                }, 2000);
            },
            error: function(err) {

                $("#create").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#create").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }

    Edit = () => {
        let data = new FormData();

        var name = $("#name").val();
        var dataCenter = $("#dataCenter").val();
        var dbHost = $("#dbHost").val();
        var dbName = $("#dbName").val();
        var tableName = $("#tableName").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var remark = $("#remark").val();

        if (name == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty name!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        var re = /\s/g;
        if (re.test(name)) {
            Swal.fire({
                title: "Validation",
                text: "Username must not contain spaces!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (dataCenter == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty data center!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (dbHost == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty dbHost!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (dbName == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty dbName!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (tableName == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty tableName!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (username == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty username!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        if (remark == "") {
            Swal.fire({
                title: "Validation",
                text: "Empty remark!",
                icon: "error",
                confirmButtonColor: "#556ee6"
            })
            return
        }

        data.append('name', name);
        data.append('dataCenter', dataCenter);
        data.append('dbHost', dbHost);
        data.append('dbName', dbName);
        data.append('tableName', tableName);
        data.append('username', username);
        data.append('password', password);
        data.append('remark', remark);


        $("#edit").html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle me-2"></i> Loading');
        $("#edit").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/configProfile/postEdit`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                Swal.fire({
                    title: "Successfuly",
                    text: "Edit config profile successfuly",
                    icon: "success",
                    confirmButtonColor: "#556ee6"
                })

                setTimeout(function() {
                    location.href = `${baseUrl}/configProfile`;
                }, 2000);
            },
            error: function(err) {

                $("#edit").html('<i class="bx bx-send font-size-20 align-middle me-2"></i> Submit');
                $("#edit").prop("disabled", false);

                Swal.fire({
                    title: "Error",
                    text: "Something went wrong, please try again",
                    icon: "error",
                    confirmButtonColor: "#556ee6"
                })
            }
        });
    }
</script>