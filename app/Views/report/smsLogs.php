<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">SMS Logs</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Report</li>
                                <li class="breadcrumb-item">SMS Logs</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3 <?= session("role") == "PARTNER" ? "filter-none" : "" ?>">
                                    <div class="mb-3">
                                        <label class="form-label">Indexs :</label>
                                        <select class="select2 form-control select2-multiple" multiple="multiple" id="indexs" data-placeholder="Choose ...">
                                            <?php foreach ($index as $row) : ?>
                                                <option data-id="<?= $row->pattern ?>" value="<?= $row->pattern ?>"><?= $row->pattern ?> <?= $row->pattern == "*" ? "(ALL)" : "" ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label class="form-label">Start :</label>
                                        <input type="date" class="form-control" id="start" value="<?= date("Y-m-d") ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label class="form-label">End :</label>
                                        <input type="date" class="form-control" id="end" value="<?= date("Y-m-d") ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label class="form-label">Search :</label>
                                        <input type="text" class="form-control" id="search" placeholder="Enter search...">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label class="form-label">Msisdn :</label>
                                        <input type="text" class="form-control" id="msisdn" placeholder="Enter msisdn...">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label class="form-label">Sid :</label>
                                        <input type="text" class="form-control" id="sid" placeholder="Enter sid...">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label class="form-label">Cpname :</label>
                                        <input type="text" class="form-control" id="cpname" placeholder="Enter cpname...">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label class="form-label">Trxid :</label>
                                        <input type="text" class="form-control" id="trxid" placeholder="Enter trxid...">
                                    </div>
                                </div>
                                <div class="col-md-3 <?= session("role") == "PARTNER" ? "filter-none" : "" ?>">
                                    <div class="mb-3">
                                        <label class="form-label">Customer :</label>
                                        <select class="select2 form-control select2-multiple" multiple="multiple" id="prefix" data-placeholder="Choose ...">
                                            <option value="">Select</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 align-self-end">
                                    <div class="mb-3">
                                        <button type="button" class="btn btn-primary w-md" onclick="Submit()" id="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="loader-table" style="padding-bottom: 30px; display:none;">
                                <img src="<?= base_url("public/assets/images/loader.gif") ?>" alt="spinner" width="80" style="position: absolute; padding-top: 71px; margin-left: 40%;">
                            </div>

                            <table id="dataSmsLogs" class="table table-bordered nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Msisdn</th>
                                        <th>Response</th>
                                        <th>Year</th>
                                        <th>Month</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Timestamp</th>
                                        <th>Sms Count</th>
                                        <th>Trxid</th>
                                        <th>Cpname</th>
                                        <th>Proc</th>
                                        <th>Path</th>
                                        <th>Url</th>
                                        <th>Host</th>
                                        <th>Sid</th>
                                        <th>Message</th>
                                        <th>SMS</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/smsLogs'); ?>