<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<style>
    th {
        text-align: center
    }

    .average {
        text-align: right
    }
</style>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Traffic Summary</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Report</li>
                                <li class="breadcrumb-item">Traffic Summary</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2 <?= session("role") == "PARTNER" ? "filter-none" : "" ?>">
                                    <div class="mb-2">
                                        <label class="form-label">Indexs :</label>
                                        <select class="select2 form-control select2-multiple" multiple="multiple" id="indexs" data-placeholder="Choose ...">
                                            <option value="">Select</option>
                                            <?php foreach ($index as $row) : ?>
                                                <option data-id="<?= $row->pattern ?>" value="<?= $row->pattern ?>"><?= $row->pattern ?> <?= $row->pattern == "*" ? "(ALL)" : "" ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" style="width: 209px;">
                                    <div class="mb-3">
                                        <label class="form-label">Start Time:</label>
                                        <input type="datetime-local" class="form-control" id="start" value="<?= date("Y-m-d") ?>T00:00">
                                    </div>
                                </div>
                                <div class="col-md-2" style="width: 209px;">
                                    <div class="mb-3">
                                        <label class="form-label">End Time:</label>
                                        <input type="datetime-local" class="form-control" id="end" value="<?= date("Y-m-d") ?>T23:59">
                                    </div>
                                </div>
                                <div class="col-md-2 <?= session("role") == "PARTNER" ? "filter-none" : "" ?>">
                                    <div class="mb-3">
                                        <label class="form-label">Customer :</label>
                                        <select class="select2 form-control select2-multiple" multiple="multiple" id="prefix" data-placeholder="Choose ...">
                                            <option value="">Select</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label class="form-label">Sid :</label>
                                        <input type="text" class="form-control" id="sid" placeholder="Enter sid...">
                                    </div>
                                </div>
                                <div class="col-md-1 align-self-end">
                                    <div class="mb-3">
                                        <button type="button" class="btn btn-primary w-md" onclick="Submit()" id="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="loader-table" style="padding-bottom: 30px; display:none;">
                                <img src="<?= base_url("public/assets/images/loader.gif") ?>" alt="spinner" width="80" style="position: absolute; padding-top: 71px; margin-left: 40%;">
                            </div>

                            <table id="data" class="table table-bordered w-100">
                                <thead>
                                    <tr>
                                        <th rowspan="3">No</th>
                                        <th rowspan="3">Customer</th>
                                        <th rowspan="3">Sender</th>
                                        <th rowspan="3">Total (Count)</th>
                                        <th colspan="4" align="center">Response</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" align="center">Success</th>
                                        <th colspan="2">Failed</th>
                                    </tr>
                                    <tr>
                                        <th>Count</th>
                                        <th>Sum</th>
                                        <th>Count</th>
                                        <th>Sum</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/trafficSummary'); ?>