<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<style>
     .average {
        text-align: right
    }
</style>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Traffic International</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Report</li>
                                <li class="breadcrumb-item">Traffic International</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label class="form-label">Start :</label>
                                        <input type="date" class="form-control" id="start" value="<?= date("Y-m-d") ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label class="form-label">End :</label>
                                        <input type="date" class="form-control" id="end" value="<?= date("Y-m-d") ?>">
                                    </div>
                                </div>
                                <div class="col-md-2 <?= session("role") == "PARTNER" ? "filter-none" : "" ?>">
                                    <div class="mb-3">
                                        <label class="form-label">Client :</label>
                                        <input type="text" class="form-control" id="client" placeholder="Enter client...">
                                    </div>
                                </div>
                                <div class="col-md-2 <?= session("role") == "PARTNER" ? "filter-none" : "" ?>">
                                    <div class="mb-3">
                                        <label class="form-label">Sender :</label>
                                        <input type="text" class="form-control" id="sender" placeholder="Enter sender...">
                                    </div>
                                </div>
                                <div class="col-md-2 align-self-end">
                                    <div class="mb-3">
                                        <button type="button" class="btn btn-primary w-md" onclick="Submit()" id="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="loader-table" style="padding-bottom: 30px; display:none;">
                                <img src="<?= base_url("public/assets/images/loader.gif") ?>" alt="spinner" width="80" style="position: absolute; padding-top: 71px; margin-left: 40%;">
                            </div>

                            <table id="data" class="table table-bordered nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Client</th>
                                        <th>Sender</th>
                                        <th>Status</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/trafficInternational'); ?>