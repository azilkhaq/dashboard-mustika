<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Edit Users</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Management Users</li>
                                <li class="breadcrumb-item">Edit Users</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Form Edit Users</h4>

                            <form>
                                <div class="image-upload">
                                    <center>
                                        <div class="col-md-3">
                                            <?php if ($data->profilePic != "") { ?>
                                                <img src="<?= base_url("uploads") . $data->profilePic ?>" alt="image" id="showImage" style="height: 270px;">
                                                <input type="text" value="<?= $data->profilePic ?>" id="valProfilePic" hidden>
                                            <?php } else { ?>
                                                <img src="<?= base_url('public/assets/images/default.gif') ?>" alt="image" id="showImage">
                                            <?php } ?>
                                            <input type="file" class="form-control" id="image">
                                        </div>
                                    </center>
                                </div>

                                <input type="text" id="userId" value="<?= $data->userId ?>" hidden>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Username</label>
                                            <input type="text" class="form-control" id="username" value="<?= $data->userId ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Fullname</label>
                                            <input type="text" class="form-control" id="fullname" value="<?= $data->fullname ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Email</label>
                                            <input type="text" class="form-control" id="email" value="<?= $data->email ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Role</label>
                                            <select id="role" class="form-control" disabled>
                                                <option value="">Select Role</option>
                                                <option value="SUPER_ADMIN" <?= $data->role == "SUPER_ADMIN" ? "selected" : "" ?>>Super Admin</option>
                                                <option value="SUPER_PARTNER" <?= $data->role == "SUPER_PARTNER" ? "selected" : "" ?>>Super Partner</option>
                                                <option value="ADMIN" <?= $data->role == "ADMIN" ? "selected" : "" ?>>Admin</option>
                                                <option value="PARTNER" <?= $data->role == "PARTNER" ? "selected" : "" ?>>Partner</option>
                                            </select>
                                        </div>
                                    </div>

                                    <?php if ($data->role == "PARTNER") { ?>
                                        <div class="partner" style="display: block !important;">
                                            <div class="row">
                                                <h5>Config Partner</h5>

                                                <div class="col-md-6">
                                                    <div class="mb-3">
                                                        <label>Client ID</label>
                                                        <input type="text" class="form-control" id="clientId" value="<?= $data->info->clientId ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="mb-3">
                                                        <label>Customer</label>
                                                        <input type="text" class="form-control" id="prefix" value="<?= $data->info->prefixes[0]->prefix ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="mb-3">
                                                        <label>Type</label>
                                                        <input type="text" class="form-control" id="typePartner" value="<?= $data->info->prefixes[0]->type ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="mb-3">
                                                        <label>Pattern</label>
                                                        <input type="text" class="form-control" id="pattern" value="<?= $data->info->prefixes[0]->pattern ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="mb-3">
                                                        <label>Remark</label>
                                                        <input type="text" class="form-control" id="remark" value="<?= $data->info->prefixes[0]->remark ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Password</label>
                                            <input type="text" class="form-control" id="password" placeholder="Enter password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Confirm Password</label>
                                            <input type="text" class="form-control" id="confirmPassword" placeholder="Enter confirm password">
                                        </div>
                                    </div>
                                </div>

                                <div class="button-form">
                                    <a href="<?= base_url('users') ?>" class="btn btn-danger w-md"><i class="bx bx-x-circle font-size-20 align-middle me-2"></i> Batal</a>&nbsp;
                                    <button type="button" class="btn btn-primary w-md" onclick="Edit()" id="edit"><i class="bx bx-send font-size-20 align-middle me-2"></i> Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/users'); ?>