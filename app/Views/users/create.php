<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Add Users</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Managament Users</li>
                                <li class="breadcrumb-item">Add Users</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Form Add Users</h4>

                            <form>
                                <div class="image-upload">
                                    <center>
                                        <div class="col-md-3">
                                            <img src="<?= base_url('public/assets/images/default.gif') ?>" alt="image" id="showImage">
                                            <input type="file" class="form-control" id="image">
                                        </div>
                                    </center>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Username</label>
                                            <input type="text" class="form-control" id="username" placeholder="Enter username">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Fullname</label>
                                            <input type="text" class="form-control" id="fullname" placeholder="Enter fullname">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Email</label>
                                            <input type="text" class="form-control" id="email" placeholder="Enter email">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Role</label>
                                            <select id="role" class="form-control">
                                                <option value="">Select Role</option>
                                                <option value="SUPER_ADMIN">Super Admin</option>
                                                <option value="SUPER_PARTNER">Super Partner</option>
                                                <option value="ADMIN">Admin</option>
                                                <option value="PARTNER">Partner</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="partner">
                                        <div class="row">
                                            <h5>Config Partner</h5>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label>Client ID</label>
                                                    <input type="text" class="form-control" id="clientId" placeholder="Enter client id">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label>Customer</label>
                                                    <input type="text" class="form-control" id="prefix" placeholder="Enter prefix">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label>Type</label>
                                                    <input type="text" class="form-control" id="typePartner" placeholder="Enter type">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label>Pattern</label>
                                                    <input type="text" class="form-control" id="pattern" placeholder="Enter pattern">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label>Remark</label>
                                                    <input type="text" class="form-control" id="remark" placeholder="Enter remark">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Password</label>
                                            <input type="text" class="form-control" id="password" placeholder="Enter password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label>Confirm Password</label>
                                            <input type="text" class="form-control" id="confirmPassword" placeholder="Enter confirm password">
                                        </div>
                                    </div>
                                </div>

                                <div class="button-form">
                                    <a href="<?= base_url('users') ?>" class="btn btn-danger w-md"><i class="bx bx-x-circle font-size-20 align-middle me-2"></i> Batal</a>&nbsp;
                                    <button type="button" class="btn btn-primary w-md" onclick="Create()" id="create"><i class="bx bx-send font-size-20 align-middle me-2"></i> Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/users'); ?>