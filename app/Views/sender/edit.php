<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Edit Sender</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Managament Sender</li>
                                <li class="breadcrumb-item">Add Edit</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Form Add Edit</h4>

                            <input type="text" id="Id" value="<?= $data->id ?>" hidden>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>CP Name</label>
                                        <input type="text" class="form-control" id="cpName" value="<?= $data->cpName ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Sender</label>
                                        <input type="text" class="form-control" id="sender" value="<?= $data->sender ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>SID</label>
                                        <input type="text" class="form-control" id="sid" value="<?= $data->sid ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Url</label>
                                        <input type="text" class="form-control" id="url" value="<?= $data->url ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-3">
                                        <label>PWD</label>
                                        <input type="text" class="form-control" id="pwd" value="<?= $data->pwd ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-3">
                                        <label>Owner</label>
                                        <input type="text" class="form-control" id="owner" value="<?= $data->owner ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-3">
                                        <label>Type</label>
                                        <input type="text" class="form-control" id="type" value="<?= $data->type ?>">
                                    </div>
                                </div>
                                <div class="button-form">
                                    <a href="<?= base_url('sender') ?>" class="btn btn-danger w-md"><i class="bx bx-x-circle font-size-20 align-middle me-2"></i> Batal</a>&nbsp;
                                    <button type="button" class="btn btn-primary w-md" onclick="Edit()" id="edit"><i class="bx bx-send font-size-20 align-middle me-2"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/sender'); ?>