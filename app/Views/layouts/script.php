<script src="<?= base_url("public/assets/libs/jquery/jquery.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/bootstrap/js/bootstrap.bundle.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/metismenu/metisMenu.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/simplebar/simplebar.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/node-waves/waves.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/apexcharts/apexcharts.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net/js/jquery.dataTables.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/jszip/jszip.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/pdfmake/build/pdfmake.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/pdfmake/build/vfs_fonts.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net-buttons/js/buttons.html5.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net-buttons/js/buttons.print.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net-buttons/js/buttons.colVis.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") ?>"></script>
<script src="<?= base_url("public/assets/js/pages/datatables.init.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/jquery.repeater/jquery.repeater.min.js") ?>"></script>
<script src="<?= base_url("public/assets/js/pages/form-repeater.int.js") ?>"></script>
<script src="<?= base_url("public/assets/js/app.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("public/assets/js/pages/sweet-alerts.init.js") ?>"></script>
<script src="<?= base_url("public/assets/libs/select2/js/select2.min.js") ?>"></script>
<script src="<?= base_url("public/assets/js/pages/ecommerce-select2.init.js") ?>"></script>

<script src="<?= base_url("public/assets/js/pages/form-advanced.init.js") ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<script>
    const baseUrl = '<?= base_url(); ?>';
    const mediaUrl = '<?= base_url("uploads"); ?>';
    const sRole = '<?= session("role") ?>';

    function ShowImage(input) {
        if (input.files && input.files[0]) {
            var editimage = new FileReader();

            editimage.onload = function(e) {
                $('#showImage').attr('src', e.target.result);
            }

            $("#showImage").css({
                "height": "270px"
            });

            editimage.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
        ShowImage(this);
    });
</script>