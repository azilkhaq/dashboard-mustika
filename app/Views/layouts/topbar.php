<?php

use Config\Services;

$request = Services::request();

$session = Services::session();

?>


<style>
  .logo-lg {
    color: white;
    font-size: 25px;
  }

  .logo-sm {
    color: white;
    font-size: 20px;
    margin-left: -17px;
  }
</style>

<header id="page-topbar">
  <div class="navbar-header">
    <div class="d-flex">
      <!-- LOGO -->
      <div class="navbar-brand-box">

        <a href="" class="logo logo-light">
          <span class="logo-sm">
            <img src="<?= base_url("public/assets/images/img-4.png") ?>" alt="" style="height: 55px; width:55px;">
          </span>
          <span class="logo-lg">
            MUSTIKA
          </span>
        </a>
      </div>

      <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
        <i class="fa fa-fw fa-bars"></i>
      </button>
    </div>

    <div class="d-flex">

      <div class="dropdown d-none d-lg-inline-block ms-1">
        <button type="button" class="btn header-item noti-icon waves-effect" data-bs-toggle="fullscreen">
          <i class="bx bx-fullscreen"></i>
        </button>
      </div>

      <div class="dropdown d-inline-block">
        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php if (session("profilePic") != "") { ?>
            <img class="rounded-circle header-profile-user" src="<?= base_url("uploads") . session("profilePic") ?>" alt="profilepic">
          <?php } else { ?>
            <img class="rounded-circle header-profile-user" src="<?= base_url("public/assets/images/ico.png") ?>" alt="profilepic">
          <?php } ?>
          <span class="d-none d-xl-inline-block ms-1" key="t-henry"><?= session("username") ?></span>
          <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-end">
          <a class="dropdown-item" href="<?= base_url("profile") ?>"><i class="bx bx-user font-size-16 align-middle me-1"></i> <span key="t-profile">Profile</span></a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item text-danger" href="<?= base_url("auth/logout") ?>"><i class="bx bx-power-off font-size-16 align-middle me-1 text-danger"></i> <span key="t-logout">Logout</span></a>
        </div>
      </div>

    </div>
  </div>
</header>