<?php

use Config\Services;

$request = Services::request();

$session = Services::session();

?>


<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">Menu</li>

                <li class="<?= $request->uri->getSegment(1) == "home" ? "mm-active" : "" ?>">
                    <a href="<?= base_url("/") ?>" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <?php if (session("role") == "SUPER_ADMIN") : ?>

                    <li class="<?= $request->uri->getSegment(1) == "users" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("users") ?>" class="waves-effect">
                            <i class="bx bx-group"></i>
                            <span>Users</span>
                        </a>
                    </li>

                    <li class="<?= $request->uri->getSegment(1) == "clients" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("clients") ?>" class="waves-effect">
                            <i class="bx bx-user-voice"></i>
                            <span>Clients</span>
                        </a>
                    </li>

                    <li class="<?= $request->uri->getSegment(1) == "sender" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("sender") ?>" class="waves-effect">
                            <i class="bx bxs-data"></i>
                            <span>Sender</span>
                        </a>
                    </li>

                    <li class="<?= $request->uri->getSegment(1) == "configProfile" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("configProfile") ?>" class="waves-effect">
                            <i class="bx bxs-user-detail"></i>
                            <span>Config Profile</span>
                        </a>
                    </li>

                    <li class="<?= $request->uri->getSegment(1) == "configSender" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("configSender") ?>" class="waves-effect">
                            <i class="bx bx-broadcast"></i>
                            <span>Config Sender</span>
                        </a>
                    </li>

                    <li class="<?= $request->uri->getSegment(1) == "report" ? "mm-active" : "" ?>">
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="bx bxs-bar-chart-alt-2"></i>
                            <span key="t-email">Report</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="<?= base_url("report/smsLogs") ?>" key="t-inbox">SMS Logs</a></li>
                            <li><a href="<?= base_url("report/trafficTime") ?>" key="t-read-email">Traffic Time</a></li>
                            <li><a href="<?= base_url("report/trafficSender") ?>" key="t-read-email">Traffic Sender</a></li>
                            <li><a href="<?= base_url("report/trafficSummary") ?>" key="t-read-email">Traffic Summary</a></li>
                            <li><a href="<?= base_url("report/trafficInternational") ?>" key="t-read-email">Traffic International</a></li>
                        </ul>
                    </li>

                <?php endif ?>

                <?php if (session("role") == "ADMIN") : ?>

                    <li class="<?= $request->uri->getSegment(1) == "clients" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("clients") ?>" class="waves-effect">
                            <i class="bx bx-user-voice"></i>
                            <span>Clients</span>
                        </a>
                    </li>

                    <li class="<?= $request->uri->getSegment(1) == "sender" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("sender") ?>" class="waves-effect">
                            <i class="bx bxs-data"></i>
                            <span>Sender</span>
                        </a>
                    </li>

                <?php endif ?>

                <?php if (session("role") == "PARTNER" || session("role") == "SUPER_PARTNER") : ?>
                    <li class="<?= $request->uri->getSegment(1) == "smsLogs" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("report/smsLogs") ?>" class="waves-effect">
                            <i class="bx bx-message-alt-dots"></i>
                            <span>SMS Logs</span>
                        </a>
                    </li>
                    <li class="<?= $request->uri->getSegment(1) == "trafficTime" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("report/trafficTime") ?>" class="waves-effect">
                            <i class="bx bx-time-five"></i>
                            <span>Traffic Time</span>
                        </a>
                    </li>
                    <li class="<?= $request->uri->getSegment(1) == "trafficSender" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("report/trafficSender") ?>" class="waves-effect">
                            <i class="bx bx-broadcast"></i>
                            <span>Traffic Sender</span>
                        </a>
                    </li>
                    <li class="<?= $request->uri->getSegment(1) == "trafficSummary" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("report/trafficSummary") ?>" class="waves-effect">
                            <i class="bx bxs-bar-chart-alt-2"></i>
                            <span>Traffic Summary</span>
                        </a>
                    </li>
                    <li class="<?= $request->uri->getSegment(1) == "trafficInternational" ? "mm-active" : "" ?>">
                        <a href="<?= base_url("report/trafficInternational") ?>" class="waves-effect">
                            <i class="bx bx-world"></i>
                            Traffic International
                        </a>
                    </li>
                <?php endif ?>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>