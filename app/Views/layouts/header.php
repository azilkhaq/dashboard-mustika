<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Dashboard Mustika</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="Dashboard Mustika" name="description" />
  <meta content="Dashboard Mustika" name="author" />
  <!-- App favicon -->
  <link rel="shortcut icon" href="<?= base_url("public/assets/images/img-4.png") ?>">
  <!-- Bootstrap Css -->
  <link href="<?= base_url("public/assets/css/bootstrap.min.css") ?>" id="bootstrap-style" rel="stylesheet" type="text/css" />

  <!-- select2 css -->
  <link href="<?= base_url("public/assets/libs/select2/css/select2.min.css") ?>" rel="stylesheet" type="text/css" />

  <!-- Icons Css -->
  <link href="<?= base_url("public/assets/css/icons.min.css") ?>" rel="stylesheet" type="text/css" />
  <!-- App Css-->
  <link href="<?= base_url("public/assets/css/app.min.css") ?>" id="app-style" rel="stylesheet" type="text/css" />

  <!-- DataTables -->
  <link href="<?= base_url("public/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css") ?>" rel="stylesheet" type="text/css" />
  <link href="<?= base_url("public/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css") ?>" rel="stylesheet" type="text/css" />

  <!-- Responsive datatable examples -->
  <link href="<?= base_url("public/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") ?>" rel="stylesheet" type="text/css" />

  <!-- Sweet Alert-->
  <link href="<?= base_url("public/assets/libs/sweetalert2/sweetalert2.min.css") ?>" rel="stylesheet" type="text/css" />

  <style>
    .dataTables_scroll {
      padding-bottom: 14px;
    }

    .row-button {
      padding-bottom: 20px;
    }

    .button-form {
      padding-top: 10px;
    }

    .image-upload {
      padding-bottom: 15px;
    }

    #showImage {
      padding-bottom: 15px;
      width: 270px;
    }

    .back-page {
      padding-top: 30px;
    }

    .back-page a {
      float: right;
    }

    .vidio {
      margin-left: -25px;
      padding-top: 5px;
    }

    .profile-div {
      padding-bottom: 20px;
    }

    .partner {
      padding-top: 10px;
      padding-bottom: 10px;
      display: none;
    }

    .filter-none {
      display: none;
    }

    .tab-button {
      padding-top: 20px;
      padding-bottom: 30px;
    }
  </style>

</head>

<body data-sidebar="dark"></body>