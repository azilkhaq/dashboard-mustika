<?= view('layouts/header'); ?>
<?= view('layouts/topbar'); ?>
<?= view('layouts/sidebar'); ?>

<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-6">
                    <h3 class="doc-section-title profile-div">Edit Profile</h3>
                    <div class="card">
                        <div class="card-body">

                            <form class="form-horizontal error">
                                <div class="row">

                                    <div class="col-md-12 mb-md profile-div">
                                        <div class="control-group">
                                            <div class="controls">
                                                <label for="fullname">Fullname </label>
                                                <input type="text" class="form-control" id="fullname" value="<?= $profile->fullname ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-md profile-div">
                                        <div class="control-group">
                                            <div class="controls">
                                                <label for="Email">Email </label>
                                                <input type="text" class="form-control" id="email" value="<?= $profile->email ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-md profile-div">
                                        <div class="control-group">
                                            <div class="controls">
                                                <label>Profile Picture </label> <br>
                                                <?php if ($profile->profilePic != "") { ?>
                                                    <img src="<?= base_url("uploads") . $profile->profilePic ?>" alt="image" id="showImage" style="height: 270px;">
                                                <?php } else { ?>
                                                    <img src="<?= base_url('public/assets/images/default.gif') ?>" alt="image" id="showImage">
                                                <?php } ?>

                                                <input type="file" class="form-control" id="image">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="<?= base_url('home') ?>" class="btn btn-danger w-md"><i class="bx bx-x-circle font-size-20 align-middle me-2"></i> Batal</a>&nbsp;
                                <button type="button" class="btn btn-primary w-md" onclick="UpdateProfile()" id="updateProfile"><i class="bx bx-send font-size-20 align-middle me-2"></i> Submit</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <h2 class="doc-section-title profile-div">Change Password</h2>
                    <div class="card">
                        <div class="card-body">

                            <form class="form-horizontal">
                                <div class="row">

                                    <div class="col-md-12 mb-md profile-div">
                                        <div class="control-group">
                                            <div class="controls">
                                                <label class="bmd-label-static">Old password</label>
                                                <input type="text" class="form-control" id="oldPassword">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-md profile-div">
                                        <div class="control-group">
                                            <div class="controls">
                                                <label class="bmd-label-static">New Password</label>
                                                <input type="text" class="form-control" id="newPassword">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-md profile-div">
                                        <div class="control-group">
                                            <div class="controls">
                                                <label class="bmd-label-static">Confirm Password</label>
                                                <input type="text" class="form-control" id="confirmPassword">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <a href="<?= base_url('home') ?>" class="btn btn-danger w-md"><i class="bx bx-x-circle font-size-20 align-middle me-2"></i> Batal</a>&nbsp;
                                <button type="button" class="btn btn-primary w-md" onclick="ChangePassword()" id="changePassword"><i class="bx bx-send font-size-20 align-middle me-2"></i> Submit</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- container-fluid -->

    </div>
    <!-- End Page-content -->
</div>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>
<?= view('js/profile'); ?>