<!-- Add Sender -->
<div id="addSender" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Form Add Sender</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label>Sender Id</label>
                                    <select class="form-control select2" id="senderId">

                                    </select>
                                </div>
                            </div>

                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                <button type="button" onclick="AddSender('<?= $clientId ?>')" id="add" class="btn btn-primary waves-effect waves-light">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- /.Add Sender -->

<!-- Add Sender -->
<div id="addProfile" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Form Add Sender</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label>Config Profile Names</label>
                                    <select class="form-control select2" id="configProfileNames">

                                    </select>
                                </div>
                            </div>

                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                <button type="button" onclick="AddProfile('<?= $clientId ?>')" id="add" class="btn btn-primary waves-effect waves-light">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- /.Add Sender -->