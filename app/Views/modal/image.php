<div id="profileImage" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Profile</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
               <img src="" alt="no image available" id="imageProfile" style="width: 100%; height: auto; max-height: 250px;">
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>