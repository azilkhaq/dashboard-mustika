<?php

use Config\Services;

$request = Services::request();

$session = Services::session();
$session->destroy();

?>

<?= view('layouts/header'); ?>

<style>
  body {
    background-image: url(../public/assets/images/auth-bg.png);
    background-size: cover;
  }
</style>

<div class="account-pages my-5 pt-sm-5">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 col-lg-6 col-xl-5">
        <div class="card overflow-hidden">
          <div class="bg-primary bg-soft">
            <div class="row">
              <div class="col-7">
                <div class="text-primary p-4">
                  <h5 class="text-primary">Selamat Datang !</h5>
                  <p>Masuk untuk melanjutkan.</p>
                </div>
              </div>
              <div class="col-5 align-self-end">
                <img src="<?= base_url("public/assets/images/profile-img.png") ?>" alt="" class="img-fluid">
              </div>
            </div>
          </div>
          <div class="card-body pt-0">
            <div class="auth-logo">
              <a href="#" class="auth-logo-dark">
                <div class="avatar-md profile-user-wid mb-4">
                  <span class="avatar-title rounded-circle bg-light">
                    <img src="<?= base_url("public/assets/images/img-4.png") ?>" alt="" class="rounded-circle" height="34" style="height: 55px; width:55px;">
                  </span>
                </div>
              </a>
            </div>
            <div class="p-2">
              <form class="form-horizontal" action="<?= base_url('auth/login') ?>" method="POST">

                <div class="mb-3">
                  <label for="username" class="form-label">Username</label>
                  <input type="text" class="form-control" name="username" placeholder="Enter username">
                </div>

                <div class="mb-3">
                  <label class="form-label">Password</label>
                  <div class="input-group auth-pass-inputgroup">
                    <input type="password" name="password" class="form-control" placeholder="Enter password" aria-label="Password" aria-describedby="password-addon">
                    <button class="btn btn-light " type="button" id="password-addon"><i class="mdi mdi-eye-outline"></i></button>
                  </div>
                </div>

                <div class="mt-3 d-grid" style="padding-top: 10px;">
                  <button class="btn btn-primary waves-effect waves-light" type="submit">Log In</button>
                </div>

                <div class="mt-4 text-center">
                  <a href="#" class="text-muted"><i class="mdi mdi-lock me-1"></i> Forgot your password?</a>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  <?= view('layouts/script'); ?>

  <?php if (session('message')) { ?>

    <?php if (session('status') == true) { ?>
      <script>
        Swal.fire({
          title: "Login Berhasil!",
          text: "<?= session('message') ?>",
          icon: "success",
          confirmButtonColor: "#556ee6"
        })
      </script>
    <?php } else { ?>
      <script>
        Swal.fire({
          title: "Login Gagal!",
          text: "<?= session('message') ?>",
          icon: "error",
          confirmButtonColor: "#556ee6"
        })
      </script>
    <?php } ?>

  <?php } ?>