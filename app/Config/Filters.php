<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

class Filters extends BaseConfig
{
	public $aliases = [
		'isLoggedIn' => \App\Filters\isLoggedIn::class,
		'csrf'     => \CodeIgniter\Filters\CSRF::class,
		'toolbar'  => \CodeIgniter\Filters\DebugToolbar::class,
		'honeypot' => \CodeIgniter\Filters\Honeypot::class,
	];

	public $globals = [
		'after'  => [
			'toolbar',
		],
	];

	public $methods = [];

	public $filters = [
		'isLoggedIn' => [
			'before' => [
				'/',
				'home',
				'users',
				'users/create',
				'configProfile',
				'configProfile/create',
				'configSender',
				'configSender/create',
				'profile',
				'report/smsLogs',
				'report/trafficTime',
				'report/trafficSender',
				'partner',
				'superPartner',
				'clients',
				'clients/create',
				'sender',
				'sender/create'
			],
			'after'  => [
				'/',
				'auth/login'
			]
		],
	];
}
