<?php

namespace Config;

$routes = Services::routes();
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
  require SYSTEMPATH . 'Config/Routes.php';
}

$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('HomeController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

$routes->group('auth', function ($routes) {
  $routes->get('login', 'LoginController::index', ['namespace' => 'App\Controllers\Auth']);
  $routes->post('login', 'LoginController::store', ['namespace' => 'App\Controllers\Auth']);
  $routes->get('logout', 'LoginController::logout', ['namespace' => 'App\Controllers\Auth']);
});

$routes->group('home', function ($routes) {
  $routes->get('/', 'HomeController::index', ['namespace' => 'App\Controllers']);
  $routes->post('getData', 'HomeController::getData', ['namespace' => 'App\Controllers']);
});

$routes->group('users', function ($routes) {
  $routes->get('/', 'UsersController::index', ['namespace' => 'App\Controllers']);
  $routes->post('getData', 'UsersController::getData', ['namespace' => 'App\Controllers']);
  $routes->post('getDetail/(:any)', 'UsersController::getDetail/$1', ['namespace' => 'App\Controllers']);
  $routes->get('create', 'UsersController::create', ['namespace' => 'App\Controllers']);
  $routes->post('postCreate', 'UsersController::postCreate', ['namespace' => 'App\Controllers']);
  $routes->get('edit/(:any)', 'UsersController::edit/$1', ['namespace' => 'App\Controllers']);
  $routes->post('postEdit', 'UsersController::postEdit', ['namespace' => 'App\Controllers']);
});

$routes->group('configProfile', function ($routes) {
  $routes->get('/', 'ConfigProfileController::index', ['namespace' => 'App\Controllers']);
  $routes->post('getData', 'ConfigProfileController::getData', ['namespace' => 'App\Controllers']);
  $routes->get('create', 'ConfigProfileController::create', ['namespace' => 'App\Controllers']);
  $routes->post('postCreate', 'ConfigProfileController::postCreate', ['namespace' => 'App\Controllers']);
  $routes->get('edit/(:any)', 'ConfigProfileController::edit/$1', ['namespace' => 'App\Controllers']);
  $routes->post('postEdit', 'ConfigProfileController::postEdit', ['namespace' => 'App\Controllers']);
});

$routes->group('configSender', function ($routes) {
  $routes->get('/', 'ConfigSenderController::index', ['namespace' => 'App\Controllers']);
  $routes->post('getData/(:any)', 'ConfigSenderController::getData/$1', ['namespace' => 'App\Controllers']);
  $routes->get('create', 'ConfigSenderController::create', ['namespace' => 'App\Controllers']);
  $routes->post('postCreate', 'ConfigSenderController::postCreate', ['namespace' => 'App\Controllers']);
});

$routes->group('profile', function ($routes) {
  $routes->get('/', 'ProfileController::index', ['namespace' => 'App\Controllers']);
  $routes->post('change-password', 'ProfileController::changePassword', ['namespace' => 'App\Controllers']);
  $routes->post('update', 'ProfileController::update', ['namespace' => 'App\Controllers']);
});

$routes->group('report', function ($routes) {
  $routes->get('smsLogs', 'ReportController::smsLogs', ['namespace' => 'App\Controllers']);
  $routes->post('getDataSmsLogs', 'ReportController::getDataSmsLogs', ['namespace' => 'App\Controllers']);

  $routes->get('trafficTime', 'ReportController::trafficTime', ['namespace' => 'App\Controllers']);
  $routes->post('getDataTrafficTime', 'ReportController::getDataTrafficTime', ['namespace' => 'App\Controllers']);

  $routes->get('trafficSender', 'ReportController::trafficSender', ['namespace' => 'App\Controllers']);
  $routes->post('getDataTrafficSender', 'ReportController::getDataTrafficSender', ['namespace' => 'App\Controllers']);

  $routes->get('trafficSummary', 'ReportController::trafficSummary', ['namespace' => 'App\Controllers']);
  $routes->post('getDataTrafficSummary', 'ReportController::getDataTrafficSummary', ['namespace' => 'App\Controllers']);

  $routes->get('trafficInternational', 'ReportController::trafficInternational', ['namespace' => 'App\Controllers']);
  $routes->post('getDataTrafficInternational', 'ReportController::getDataTrafficInternational', ['namespace' => 'App\Controllers']);

  $routes->post('getPrefix', 'ReportController::getPrefix', ['namespace' => 'App\Controllers']);
});

$routes->group('clients', function ($routes) {
  $routes->get('/', 'ClientsController::index', ['namespace' => 'App\Controllers']);
  $routes->post('getData', 'ClientsController::getData', ['namespace' => 'App\Controllers']);
  $routes->get('detail/(:any)', 'ClientsController::detail/$1', ['namespace' => 'App\Controllers']);
  $routes->post('getDetail/(:any)', 'ClientsController::getDetail/$1', ['namespace' => 'App\Controllers']);
  $routes->get('create', 'ClientsController::create', ['namespace' => 'App\Controllers']);
  $routes->post('postCreate', 'ClientsController::postCreate', ['namespace' => 'App\Controllers']);
  $routes->get('edit/(:any)', 'ClientsController::edit/$1', ['namespace' => 'App\Controllers']);
  $routes->post('postEdit', 'ClientsController::postEdit', ['namespace' => 'App\Controllers']);
  $routes->post('delete', 'ClientsController::delete', ['namespace' => 'App\Controllers']);

  $routes->post('postAddSender', 'ClientsController::postAddSender', ['namespace' => 'App\Controllers']);
  $routes->post('deleteSender', 'ClientsController::deleteSender', ['namespace' => 'App\Controllers']);

  $routes->post('postAddProfile', 'ClientsController::postAddProfile', ['namespace' => 'App\Controllers']);
  $routes->post('deleteProfile', 'ClientsController::deleteProfile', ['namespace' => 'App\Controllers']);
});

$routes->group('sender', function ($routes) {
  $routes->get('/', 'SenderController::index', ['namespace' => 'App\Controllers']);
  $routes->post('getData', 'SenderController::getData', ['namespace' => 'App\Controllers']);
  $routes->get('create', 'SenderController::create', ['namespace' => 'App\Controllers']);
  $routes->post('postCreate', 'SenderController::postCreate', ['namespace' => 'App\Controllers']);
  $routes->get('edit/(:any)', 'SenderController::edit/$1', ['namespace' => 'App\Controllers']);
  $routes->post('postEdit', 'SenderController::postEdit', ['namespace' => 'App\Controllers']);
  $routes->post('delete', 'SenderController::delete', ['namespace' => 'App\Controllers']);
});


if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
  require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
