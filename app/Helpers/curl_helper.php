<?php

use Config\Services;

function curlHelper($url = '', $method = 'GET', $fields = [])
{
  $curl = curl_init();
  $session = Services::session();
  $token = $session->get('token');
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  if ($method === 'POST' || $method === 'PUT' || $method === "PATCH") {
    $template = "";
    $values = $fields;
    $keys = array_keys($fields);
    for ($i = 0; $i < count($keys); $i++) {
      $template .= $keys[$i] . '=' . $values[$keys[$i]] . '&';
      $query_string = substr($template, 0, -1);
    }
    curl_setopt($curl, CURLOPT_POSTFIELDS, $query_string);
  }
  curl_setopt($curl, CURLOPT_VERBOSE, true);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
  // SSL Certificate Problem : Self Signed Certificate 
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, [
    'Authorization: Bearer ' . $token,
  ]);
  $result = curl_exec($curl);
  $resultDecoded = json_decode($result);

  curl_close($curl);
  return $resultDecoded;
}

function curlImageHelper($url, $data)
{
  $session = Services::session();
  $token = $session->get('token');
  $headers = ["Content-Type : application/json", "Authorization: Basic aW5vdmFzaTc4Omlub3Zhc2k3OCE="];
  $postfields = [
    "directory" => "megakonten",
    "file" => curl_file_create($data['file']['tmp_name'], $data['file']['type'], basename($data['file']['name']))
  ];
  $curl = curl_init();
  $options = [
    CURLOPT_URL => $url,
    CURLOPT_POSTFIELDS => $postfields,
    CURLOPT_HTTPHEADER => $headers,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_SSL_VERIFYHOST => 0
  ];
  curl_setopt_array($curl, $options);
  $result = curl_exec($curl);
  $decoded = json_decode($result);
  return $decoded;
}

function searchForId($id, $array)
{
  foreach ($array as $key => $val) {
    if ($val['pattern'] === $id) {
      return $key;
    }
  }

  return null;
}


function searchForSender($id, $array)
{
  foreach ($array as $key => $val) {
    if ($val->id === $id) {
      return true;
    }
  }

  return false;
}
